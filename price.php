<?php
/*
Template Name: Price Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />

  <title>Services</title>
</head>
<body>

<header>
  <div class="compact-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xl-6">
          <h1><span class="line">Сроки и цены</span></h1>
        </div>
        <div class="col-md-12 col-xl-6 menu-wrapper">
          <nav class="menu">
            <ul>
              <li class="menu-item" id="filter-0"><a>Суды</a></li>
              <li class="menu-item" id="filter-1"><a>Экспертиза</a></li>
              <li class="menu-item" id="filter-2"><a>Поддержка</a></li>
              <li class="menu-item" id="filter-3"><a>Другие</a></li>
              <li class="menu-item activ" id="filter-all"><a>Все</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-12">
          <form class="search" method="get">
            <input type="text" name="query" placeholder="Поиск по нашим услугам" value="<?php echo $_GET["query"] ?>">
          </form>
        </div>
      </div>
    </div>
  </div>
</header>

<?php
$data = array(array(), array(), array(), array());
$acceptedPosts = get_posts(array(
  "post_type" => "service",
  "numberposts" => "-1",
  "meta_query" => array(
    "relation" => "OR",
    array(
      'key' => 'service-teaser-description',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'service-price',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'protect-right',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'teaser-title',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'service-about-content',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'protect-left',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-court-1',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-court-2',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-court-3',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-court-4',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-FAS-1',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'service-time',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-FAS-2',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-FAS-3',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    ),
    array(
      'key' => 'work-FAS-4',
      'value' => $_GET["query"],
      'compare' => 'LIKE'
    )
  )));

function isIDAccepted($id) {
  global $acceptedPosts;
  foreach ($acceptedPosts as $post)
    if ($post->ID == $id)
      return true;
    return false;
}

$serviceBlocks = get_posts(array("post_type" => "service-block", "numberposts" => "-1"));
$sections = array(array(), array(), array(), array());
foreach ($serviceBlocks as $block)
  array_push( $sections[ get_post_meta( $block->ID, "service-block-section", true ) ], $block);
$sectionNames = array("Суды и споры", "Правовая экспертиза, оценка и диагностика", "Поддержка стартапов", "Другие услуги");

$a = 0;
while ($a < count($sections)) { // remove empty sections
  if ( count( $sections[ $a ] ) == 0 ) {
    array_splice( $sections, $a, 1 );
  } else {
    $a ++;
  }
}

foreach ($sections as $sectionIndex => $section) { // remove blocks if they have no accepted IDs
  for ($i = 0; $i < count($section); $i++) {
    $ids = json_decode(get_post_meta($section[$i]->ID, "service-block-services-data", true));
    $remove = true;
    foreach ($ids as $index => $id) {
      if (isIDAccepted($id)) {
        $remove = false;
        break;
      }
    }
    if ($remove)
      unset($sections[$sectionIndex][$i]);
  }
//  echo "<pre>" . var_dump($section) . "</pre>";
}
// !IMPORTANT: Initially empty sections should not be shown, they're removed.
// All the other sections with removed blocks (for example, because of complicated search query)
// are shown with lack of results message.

for ($i = 0; $i < count($sections); $i++):
  ?>

  <section class="table-price color-control" id="section-<?php echo $i ?>">
    <h2><?php echo $sectionNames[$i] ?></h2>
    <div class="container">
      <?php if (count($sections[$i]) > 0):
        foreach ($sections[$i] as $block): ?>
        <div class="table-responsive">
          <table class="table">
            <thead>
            <tr>
              <th class="headline"><?php echo get_the_title($block->ID) ?></th>
              <th class="time">Срок</th>
              <th class="cost">Стоимость</th>
              <th class="pay"></th>
              <th class="request">
                <a href="<?php echo get_post_meta($block->ID, "service-block-pdf-file-uri", true) ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/download.svg" alt=""></a>
                <a href="<?php echo get_post_meta($block->ID, "service-block-print-version-uri", true) ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/print.svg" alt=""></a>
              </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $ids = json_decode(get_post_meta($block->ID, "service-block-services-data", true));
            foreach ($ids as $id):
              if (isIDAccepted($id)) :?>
              <tr>
                <td><?php $t = get_post_meta($id, "teaser-title", true); if ($t) echo $t; else echo "-"; ?></td>
                <td><?php $t = get_post_meta($id, "service-time", true); if ($t) echo $t; else echo "Не указан"; ?></td>
                <td><?php $t = get_post_meta($id, "service-price", true); if ($t) echo $t; else echo "Не указана"; ?></td>
                <td><a href="<?php echo get_post_meta($id, "online-payment-link", true) ?>">Оплатить онлайн</a></td>
                <td><a href="">Оставить заявку</a></td>
                <!--          TODO: заявка -->
              </tr>
            <?php endif; endforeach; ?>
            </tbody>
          </table>
        </div>
      <?php endforeach; else: ?>
      <p style="width: 100%; color: grey; text-align: center; font-size: calc(14px + 1vw);">В этой категории нет услуг по вашему запросу</>
      <?php endif; ?>
    </div>
  </section>

  <?php if ($i == count($sections)-2 && $i != 0): ?>
  <section class="get-price color-control">
    <div class="container">
      <span class="headline">Скачайте нашу презентацию и общий прайс</span>
      <p class="description">Чтобы больше узнать о нас или подготовить информацию для руководителя, скачайте презентацию о нашей компании и ее услугах</p>
      <form action="#">
        <input type="text" placeholder="Ваш E-mail">
        <button>Получить прайс</button>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
          <label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="#">политикой конфиденциальности</a></label>
        </div>
      </form>
    </div>
  </section>
  <?php endif; ?>
<?php endfor; ?>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>

<style>
  .menu-item {
    cursor: pointer;
  }
  .color-control {
    background: white;
  }
  .color-control:nth-child(2n) {
    background: #f8f3f5;
  }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }

    let filters = {
      0: $("#filter-0"),
      1: $("#filter-1"),
      2: $("#filter-2"),
      3: $("#filter-3"),
      "all": $("#filter-all")
    };

    function changeActiv(el) {  // Sets "active" class to menu elements
      for (let i in filters)
        filters[i].removeClass("activ");
      el.addClass("activ");
    }

    function changeDisplay(mode) {  // Hides every block and displays some of them
      let sections = [$("#section-0"), $("#section-1"), $("#section-2"), $("#section-3")];
      for (let i in sections) {
        if (+i === +mode || +mode === -1)
          sections[i].show();
        else
          sections[i].hide();
      }
    }

    filters[0].click(() => {
      changeActiv(filters[0]);
      changeDisplay(0);
    });
    filters[1].click(() => {
      changeActiv(filters[1]);
      changeDisplay(1);
    });
    filters[2].click(() => {
      changeActiv(filters[2]);
      changeDisplay(2);
    });
    filters[3].click(() => {
      changeActiv(filters[3]);
      changeDisplay(3);
    });
    filters["all"].click(() => {
      changeActiv(filters["all"]);
      changeDisplay(-1);
    });

  });


</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

</body>
</html>