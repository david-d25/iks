<?php
/*
Template Name: Media Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />

  <title>Services</title>
</head>
<body>

<header>
  <div class="compact-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-xl-6">
          <h1><span class="line">Медиа-центр</span></h1>
        </div>
        <div class="col-lg-12 col-xl-6 menu-wrapper">
          <nav class="menu">
            <ul>
              <li class="menu-item" id="filter-0"><a>Отзывы</a></li>
              <li class="menu-item" id="filter-1"><a>Instagram</a></li>
              <li class="menu-item" id="filter-2"><a>Статьи</a></li>
              <li class="menu-item activ"  id="filter-all"><a>Все</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-12">
          <form class="search" method="get">
            <input type="text" name="query" placeholder="Поиск по медиа-центру">
          </form>
        </div>
      </div>
    </div>
  </div>
</header>

<div id="cards-sections-wr"></div>

<?php
include "templates/contacts-section.php";
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();


</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>


<script type="text/javascript">
  let query = "<?php echo $_GET['query'] ?>";
  function generateInstagramPhotos(index, callback) {
    $.ajax(baseURL + "/backend/media-cards.php", {
      method: "POST",
      data: {index: index, query: query},
    }).done((response) => {
      $("#cards-sections-wr").append(response);
      callback();
    });
  }
  const baseURL = "<?php echo get_template_directory_uri() ?>";
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }

    let lastMode = -1;
    let filters = {
      0: $("#filter-0"),
      1: $("#filter-1"),
      2: $("#filter-2"),
      "all": $("#filter-all")
    };

    function changeActiv(el) {  // Sets "active" class to menu elements
      for (let i in filters)
        filters[i].removeClass("activ");
      el.addClass("activ");
    }

    function changeDisplay(mode) {  // Hides every block and displays some of them
      if (+mode === -2) mode = lastMode;
      let cards = [$(".card-review"), $(".card-instagram"), $(".card-post")];
      for (let i in cards) {
        if (+i === +mode || +mode === -1)
          cards[i].show();
        else
          cards[i].hide();
      }
      lastMode = mode;
    }

    filters[0].click(() => {
      changeActiv(filters[0]);
      changeDisplay(0);
    });
    filters[1].click(() => {
      changeActiv(filters[1]);
      changeDisplay(1);
    });
    filters[2].click(() => {
      changeActiv(filters[2]);
      changeDisplay(2);
    });
    filters["all"].click(() => {
      changeActiv(filters["all"]);
      changeDisplay(-1);
    });

    generateInstagramPhotos(0, () => {
      generateInstagramPhotos(1, () => {
        changeDisplay(-2);
        generateInstagramPhotos(2, () => {
          changeDisplay(-2);
        })
      })
    });

    function findGetParameter(parameterName) {
      let result = null,
        tmp = [];
      location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
      return result;
    }

    let filter = findGetParameter("filter");
    if (filter && filters.hasOwnProperty(filter))
      filters[filter].click();
  });




</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

</body>
</html>
