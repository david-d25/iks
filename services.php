<?php
/*
Template Name: Services Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />

  <title>Services</title>
</head>
<body>

<header>
  <div class="compact-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xl-6">
          <h1><span class="line">Услуги</span></h1>
        </div>
        <div class="col-md-12 col-xl-6 menu-wrapper">
          <nav class="menu">
            <ul>
              <li class="menu-item" id="filter-0"><a href="#">Суды</a></li>
              <li class="menu-item" id="filter-1"><a href="#">Экспертиза</a></li>
              <li class="menu-item" id="filter-2"><a href="#">Стартапам</a></li>
              <li class="menu-item" id="filter-3"><a href="#">Абонентские</a></li>
              <li class="menu-item activ" id="filter-all"><a href="#">Все</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-12">
          <form class="search" method="get">
            <input type="text" name="query" placeholder="Поиск по нашим услугам">
          </form>
        </div>
      </div>
    </div>
  </div>
</header>

<?php
$data = array(array(), array(), array(), array());
$posts = get_posts(array(
        "post_type" => "service",
        "numberposts" => "-1",
        "meta_query" => array(
                "relation" => "OR",
          array(
            'key' => 'service-teaser-description',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
	        array(
		        'key' => 'service-price',
		        'value' => $_GET["query"],
		        'compare' => 'LIKE'
	        ),
          array(
            'key' => 'service-time',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'protect-right',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'teaser-title',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'service-about-content',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'protect-left',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-court-1',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-court-2',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-court-3',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-court-4',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-FAS-1',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-FAS-2',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-FAS-3',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          ),
          array(
            'key' => 'work-FAS-4',
            'value' => $_GET["query"],
            'compare' => 'LIKE'
          )
        )));

foreach ( $posts as $post )
  array_push( $data[ get_post_meta( $post->ID, "service-section", true ) ], $post );

foreach ($data as $index => $blocks):
	?>
<section class="cards-group <?php if ($index%2 == 0) echo 'bg-color'; else echo 'bg-white' ?>" id="section-<?php echo $index ?>">
  <h2><?php echo array("Суды и споры", "Правовая экспертиза", "Поддержка стартапов", "Абонентское обслуживание")[$index] ?></h2>
  <div class="container">
    <div class="row mobile-slider">
      <?php if (count($blocks)>0):
        foreach ($blocks as $service): ?>
          <a class="col-md-6 col-xl-4" href="<?php echo get_permalink($service->ID) ?>" style="color: inherit; text-decoration: inherit;">
            <div class="card">
              <img class="card-img-top" src="<?php echo get_post_meta($service->ID, "teaser-block-icon-uri", true) ?>" alt="Card image cap">
              <div class="card-body">
                <h3 class="card-title"><?php echo get_post_meta($service->ID, "teaser-title", true) ?></h3>
                <ul class="card-list">
                  <?php echo get_post_meta($service->ID, "service-teaser-description", true) ?>
                </ul>
              </div>
            </div>
          </a>
        <?php endforeach;
      else:
        ?>
        <span style="text-align: center; width: 100%; font-size: 1.5em; color: grey">В этом разделе услуг не найдено</span>
      <?php
      endif;?>
    </div>
  </div>
</section>
<?php endforeach; ?>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();


</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }

    let filters = {
      0: $("#filter-0"),
      1: $("#filter-1"),
      2: $("#filter-2"),
      3: $("#filter-3"),
      "all": $("#filter-all")
    };

    function changeActiv(el) {  // Sets "active" class to menu elements
      for (let i in filters)
        filters[i].removeClass("activ");
      el.addClass("activ");
    }

    function changeDisplay(mode) {  // Hides every block and displays some of them
      let sections = [$("#section-0"), $("#section-1"), $("#section-2"), $("#section-3")];
      for (let i in sections) {
        if (+i === +mode || +mode === -1)
          sections[i].show();
        else
          sections[i].hide();
      }
    }

    filters[0].click(() => {
      changeActiv(filters[0]);
      changeDisplay(0);
    });
    filters[1].click(() => {
      changeActiv(filters[1]);
      changeDisplay(1);
    });
    filters[2].click(() => {
      changeActiv(filters[2]);
      changeDisplay(2);
    });
    filters[3].click(() => {
      changeActiv(filters[3]);
      changeDisplay(3);
    });
    filters["all"].click(() => {
      changeActiv(filters["all"]);
      changeDisplay(-1);
    });

    function findGetParameter(parameterName) {
      var result = null,
        tmp = [];
      location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
      return result;
    }

    let filter = findGetParameter("filter");
    if (filter && filters.hasOwnProperty(filter))
      filters[filter].click();
  });
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

</body>
</html>