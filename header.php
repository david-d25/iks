<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
<div class="demo-5">
  <div class="content-row content--demo-5">
    <div class="content">
      <div class="global-menu">
        <div class="row justify-content-between navbar">
          <div class="col-5">
          </div>
          <div class="col-2 navbar_logotype">
            <img class="logo global-menu__item" src="<?php echo get_template_directory_uri() ?>/img/icons/logotype.svg" alt="">
          </div>
          <div class="col-5 navbar_phone">
            <p class="phone global-menu__item"><img src="<?php echo get_template_directory_uri() ?>/img/phone-red.png" alt=""> <a href="tel:<?php echo get_option('iks-company-phone'); ?>"><?php echo get_option('iks-company-phone'); ?></a></p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <form class="search-menu" action="/media">
              <input class="global-menu__item" name="query" type="text" placeholder="Поиск по сайту">
            </form>
          </div>
        </div>
        <div class="row">
          <?php
          $blocks = json_decode(get_option("iks-main-menu-data"));
          for ($i = 0; $i < min(4, count($blocks)); $i++): ?>
          <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="global-menu__wrap">
              <div class="global-menu__item">
                <h1 class="line"><?php echo $blocks[$i]->blockTitle ?></h1>
              </div>
              <ul>
                <?php foreach ($blocks[$i]->itemlinks as $itemlink): ?>
                <li><a class="global-menu__item" href="<?php echo $itemlink->link ?>"><?php echo $itemlink->title ?></a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <?php endfor; ?>
        </div>

        <div class="row">
          <?php
          if (count($blocks) > 4)
            for($i = 4; $i < count($blocks); $i++): ?>
          <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="global-menu__wrap">
              <div class="global-menu__item">
                <h1 class="line"><?php echo $blocks[$i]->blockTitle ?></h1>
              </div>
              <ul>
                <?php foreach ($blocks[$i]->itemlinks as $itemlink): ?>
                  <li><a class="global-menu__item" href="<?php echo $itemlink->link ?>"><?php echo $itemlink->title ?></a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <?php endfor; ?>
        </div>

        <div class="row">
          <div class="social-icons global-menu__item">
            <ul>
              <li><a href="https://wa.me/<?php echo get_option("iks-whatsapp") ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/whatsapp.svg" alt=""></a></li>
              <li><a href="skype:<?php echo get_option("iks-skype") ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/skype.svg" alt=""></a></li>
              <li><a href="<?php echo get_option("iks-youtube") ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/youtube.svg" alt=""></a></li>
              <li><a href="https://www.instagram.com/<?php echo get_option("iks-instagram") ?>"><img src="<?php echo get_template_directory_uri() ?>/img/icons/instagram.svg" alt=""></a></li>
            </ul>
          </div>
        </div>

      </div>

      <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
        <path class="shape-overlays__path"></path>
        <path class="shape-overlays__path"></path>
      </svg>
    </div>
  </div>
</div>
<header class="main main--demo-5">
	<div class="hamburger hamburger--demo-5 js-hover"></div>
	<div class="header">
		<div class="row justify-content-between navbar">
			<div class="col-5 navbar_menu">

				<nav>
					<ul>
            <?php
            $nav_items = wp_get_nav_menu_items("main");
            foreach ($nav_items as $item): ?>
						<li><a href="<?php echo $item->url ?>"><?php echo $item->title ?></a></li>
            <?php endforeach; ?>
					</ul>
				</nav>
			</div>
			<div class="col-2 navbar_logotype">
				<img class="logo" src="<?php echo get_template_directory_uri() ?>/img/icons/logotype.svg" alt="">
			</div>
			<div class="col-5 navbar_phone">
				<p class="phone"><img src="<?php echo get_template_directory_uri() ?>/img/icons/phone.png" alt=""> <a href="tel:<?php echo get_option('iks-company-phone'); ?>"><?php echo get_option('iks-company-phone'); ?></a></p>
			</div>
		</div>
	</div>
</header>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }
  });


</script>

<script>
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

<script>
  $(function(){
    $("#number").mask("8(999) 999-9999");
  });
</script>