<?php
/*
Template Name: Team Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <title>Команда</title>
</head>
<body>

<header>
  <div class="alternative-header">
    <div class="container">
      <div class="row">
        <div class="col-12 banner">
          <img src="<?php echo get_template_directory_uri() ?>/img/alternative-banner1.png" alt="">
          <h1>Команда</h1>
        </div>
	      <?php
	      $currentPageSlug = 'team';
	      include 'templates/statics-navbar.php';
	      ?>
      </div>
    </div>
  </div>
</header>


<section class="team">
  <div class="container">
    <div class="row">
      <div class="col-12 headline  wow fadeIn">
        <h2><span class="line">У нас работает</span>
          <span class="line">команда</span>
          <span class="line">опытных</span>
          <span class="line">профессионалов</span></h2>
        <p>Несколько предложений о команде и ее достижениях</p>
      </div>
      <div class="col-lg-6 col-xl-3 item wow fadeIn">
        <img src="<?php echo get_template_directory_uri() ?>/img/56.png" alt="">
        <p>клиентов, из которых 80% — это бизнесмены либо корпоративные клиенты</p>
      </div>
      <div class="col-lg-6 col-xl-3 item wow fadeIn">
        <img src="<?php echo get_template_directory_uri() ?>/img/500.png" alt="">
        <p>судов по корпоративным и частным вопросам выиграли за 11 лет</p>
      </div>
      <div class="col-lg-6 col-xl-3 item wow fadeIn">
        <img src="<?php echo get_template_directory_uri() ?>/img/56.png" alt="">
        <p>клиентов, из которых 80% — это бизнесмены либо корпоративные клиенты</p>
      </div>
      <div class="col-lg-6 col-xl-3 item wow fadeIn">
        <img src="<?php echo get_template_directory_uri() ?>/img/11.png" alt="">
        <p>лет помогаем корпоративным и частным клиентам защищать свои законные интересы</p>
      </div>
    </div>
  </div>
</section>
<section class="workers">
  <?php
  $posts = get_posts(array("post_type" => "employee", "numberposts" => "-1"));
  foreach ($posts as $index => $post):
  ?>
  <div class="<?php if ($index%2 == 0) echo 'right'; else echo 'left'; ?>">
    <div class="container">
      <div class="row">
        <img src="<?php echo get_post_meta($post->ID, "employee-image-uri", true) ?>" style="max-width: 45%;" alt="">
        <div class="col-12 worker">
          <div class="info wow <?php if ($index%2 == 0) echo "slideInLeft"; else echo "slideInRight"; ?>">
            <span class="name"><?php echo get_post_meta($post->ID, "employee-name", true) ?></span>
            <p class="position"><?php echo get_post_meta($post->ID, "employee-position", true) ?></p>
            <?php if (get_post_meta($post->ID, "employee-experience", true)): ?>
              <p class="experience"><strong>Стаж работы:</strong> <?php echo get_post_meta($post->ID, "employee-experience", true) ?></p>
            <?php endif; ?>
            <p class="biography"><strong>Краткая биография:</strong> <?php echo get_post_meta($post->ID, "employee-bio", true) ?></p>
            <a href="#">Читать подробнее</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>