<?php
/*
Template Name: Jobs Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">

  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <title>Вакансии</title>
</head>
<body>

<header>
  <div class="alternative-header">
    <div class="container">
      <div class="row">
        <div class="col-12 banner">
          <img src="<?php echo get_template_directory_uri() ?>/img/alternative-banner5.png" alt="">
          <h1>Вакансии</h1>
        </div>
	      <?php
	      $currentPageSlug = 'jobs';
	      include 'templates/statics-navbar.php';
	      ?>
      </div>
    </div>
  </div>
</header>



<section class="vacancy">
  <div class="vacantion-info-wrapper">
    <div class="vacancy-info">
      <img src="<?php echo get_template_directory_uri() ?>/img/team.png" alt="">
      <div class="content fix-bullets">
        <span class="title">Работа в ИнвестКонсалт Системс - это:</span>
        <?php echo get_option("iks-vacancies-work-is") ?>
      </div>
    </div>
  </div>

  <div class="accordion" id="accordionExample">
    <?php
    $vacancies = get_posts(array("post_type" => "vacancy", "numberposts" => "-1"));
    foreach ($vacancies as $index => $vacancy):
    ?>
    <div class="card wow fadeIn">
      <div class="card-header" id="<?php echo "heading-" . $index ?>">
        <h5 class="mb-0 container">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo "collapse-" . $index ?>" aria-expanded="<?php if ($index == 0) echo 'true'; else echo 'false' ?>" aria-controls="collapseOne">
            <?php echo get_the_title($vacancy->ID) ?>
          </button>
        </h5>
      </div>

      <div id="<?php echo "collapse-" . $index ?>" class="collapse <?php if ($index == 0) echo 'show' ?> container" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="nav-wrapper">
          <nav>
            <ul class="nav nav-pills" >
              <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Условия</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Обязанности</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Требования</a>
              </li>
            </ul>
          </nav>

        </div>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active fix-bullets" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <?php echo get_post_meta($vacancy->ID, "vacancy-conditions-content", true) ?>

            <form action="#">
              <div class="form-group">
                <input type="text" placeholder="Телефон">
                <div class="custom-file" id="customFile">
                  <input type="file" class="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp">
                </div>
                <button>Перезвоните мне</button>
              </div>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
                <label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="#">политикой конфиденциальности</a></label>
              </div>
            </form>
          </div>
          <div class="tab-pane fade fix-bullets" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <?php echo get_post_meta($vacancy->ID, "vacancy-responsibilities-content", true) ?>
          </div>
          <div class="tab-pane fade fix-bullets" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
	          <?php echo get_post_meta($vacancy->ID, "vacancy-requirements-content", true) ?>
          </div>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script defer>
  new WOW().init();
  $(".fix-bullets ul li").each(function (index) {
    $(this).html(`<span>${$(this).html()}</span>`);
  });
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>