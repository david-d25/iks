<?php
/*
Template Name: Contacts Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />
  <title>Article</title>
</head>
<body>

<?php get_header() ?>

<header>

  <div class="alternative-header">
    <div class="container">
      <div class="row">
        <div class="col-12 banner">
          <img src="<?php echo get_template_directory_uri() ?>/img/alternative-banner4.png" alt="">
          <h1>Контакты</h1>
        </div>
	      <?php
	      $currentPageSlug = 'contacts';
	      include 'templates/statics-navbar.php';
	      ?>
      </div>
    </div>
  </div>
</header>

<section class="feedback">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <h2><span class="line">Как с нами</span>
          <span class="line">связаться</span></h2>

        <p class="phone"><strong>Звоните по будням, с 9 до 18:00</strong></br>
          <a href="#">7 (495) 782-82-46</a> (Москва)</p>

        <ul class="social-icons">
          <li class="icon"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/whatsapp.svg" alt=""></a></li>
          <li class="icon"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/instagram.svg" alt=""></a></li>
          <li class="icon"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/skype.svg" alt=""></a></li>
        </ul>

        <p class="address"><strong>Пишите на почту или имейл:</strong></br>
          119331, Москва, пр. Вернадского, 29</br>
          <a href="#">info@consult-systems.ru</a></p>

        <p><strong>Наши реквизиты:</strong></p>
        <ul class="requisites">
          <li>Лицензия: 123456</li>
          <li>ИНН: 7702070139</li>
          <li>КПП: 770943002</li>
          <li>БИК: 044525411</li>
        </ul>
      </div>
      <div class="col-6">
        <img src="<?php echo get_template_directory_uri() ?>/img/building.png" alt="">
      </div>
    </div>
  </div>
</section>

<section class="passage">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2><span class="line">Как доехать</span></h2>
        <a class="download" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/download2.png" alt="">Скачать схему проезда</a>
      </div>
      <div class="col-12">
        <div class="map">

          <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A750f78651c0fe9e91b03f9d4816d9c10b4472955164559eba048bc05e5cc7216&amp;width=100%25&amp;height=614&amp;lang=ru_RU&amp;scroll=true"></script>

        </div>
        <h3>Как дойти от станции метро:</h3>
        <h4><img src="<?php echo get_template_directory_uri() ?>/img/metro.png" alt="">Вернадского</h4>
        <div class="row path">
          <div class="col-6">
            <img src="<?php echo get_template_directory_uri() ?>/img/1.png" alt="">
            <p>Последний вагон из центра. Выходите из метро, поворачиваете направо и переходите дорогу, далее проехать одну остановку на автобусе №224 до остановки Проспект Вернадского, 33. Прямо напротив остановки находится вход в ТОЦ “Лето” (большие стеклянные двери).</p>
          </div>
          <div class="col-6">
            <img src="<?php echo get_template_directory_uri() ?>/img/2.png" alt="">
            <p>Последний вагон из центра. Выходите из метро, поворачиваете направо и переходите дорогу, далее проехать одну остановку на автобусе №224 до остановки Проспект Вернадского, 33. Прямо напротив остановки находится вход в ТОЦ “Лето” (большие стеклянные двери).</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

  var options = {
    offset: 1080
  }

  var header = new Headhesive('.header', options);
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }
  });


</script>

<script>
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.maskedinput.min.js"></script>
<script>
  $(function(){
    $("#number").mask("8(999) 999-9999");
  });
</script></body>
</html>