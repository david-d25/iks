<?php
/*
Template Name: About Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <title>Team</title>
</head>
<body>

<header>
  <div class="alternative-header">
    <div class="container">
      <div class="row">
        <div class="col-12 banner">
          <img src="<?php echo get_template_directory_uri() ?>/img/alternative-banner6.png" alt="">
          <h1>О компании</h1>
        </div>
        <?php
        $currentPageSlug = 'about';
        include 'templates/statics-navbar.php';
        ?>
      </div>
    </div>
  </div>
</header>

<section class="mission">
  <div class="container">
    <div class="row">
      <div class="col-10">
        <h2><span class="line">Наша</span>
          <span class="line">миссия</span></h2>
        <p class="description">Грамотное правовое сопровождение как инструмент развития бизнеса, достижения финансовых результатов и минимизации рисков</p>
        <p>Юридическая компании «ИнвестКонсалт Системс» основана в 2007 году командной профессиональных юристов.</p>
        <p>Наша главная задача — оказание качественных юридических услуг. Для нас важен тот результат, которого хочет добиться клиент. Специалисты ИКС приложат все усилия чтобы минимизировать Ваши расходы и с комфортом разрешить все возникшие трудности.</p>
        <p>Не важно, в каком бизнесе работаете, и в какой стране находитесь. Мы предоставляем юри дические услуги на территории РФ всем, кто ценит качество нашей работы. Наши юристы всегда готовы быть рядом и предоставить юридическую поддержку и защиту.</p>
        <p>За годы работы более 500 судов, вы скажете это немного, но мы стремимся не к количеству, а к качеству оказываемых нами услуг</p>

      </div>
      <div class="col-2">
        <div class="row">
          <div class="col-12">
            <img src="<?php echo get_template_directory_uri() ?>/img/56.png" alt="">
            <p>судов по корпоративными и частным выиграли за 11 лет</p>
          </div>
          <div class="col-12">
            <img src="<?php echo get_template_directory_uri() ?>/img/500.png" alt="">
            <p>судов по корпоративными и частным выиграли за 11 лет</p>
          </div>
          <div class="col-12">
            <img src="<?php echo get_template_directory_uri() ?>/img/11.png" alt="">
            <p>судов по корпоративными и частным выиграли за 11 лет</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="mission">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2><span class="line">Видиопрезентация</span>
          <span class="line">ICS</span></h2>
      </div>
      <div class="col-12">
        <div class="play">
          <img src="#" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>
<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>