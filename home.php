<!doctype html>
<html lang="ru">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta property="og:title" content="ИнвестКонсалт Системс" />
  <meta property="og:type" content="website" />
  <meta property="og:description" content="ИнвестКонсалт Системс" />
  <meta property="og:site_name" content="ИнвестКонсалт Системс" />
  <meta property="og:url" content="http://example.ru" />
  <meta property="og:image" content="http://46.148.188.195:7879/img/icons/logotype.svg" />

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />

  <title>Main pages</title>
</head>
<body>
<?php get_header() ?>

<div id="wrapper">
  <div class="slider">
	  <?php include 'templates/mainpage-slider.php' ?>
  </div>


  <section>
    <div class="row protection">
      <h2 class="protection_headline wow fadeIn"><span class="line">Защищаем</span><span class="line">интересы</span>
        <span class="line">клиентов</span></h2>
      <div class="col-xl-9 col-md-12">
        <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-arbitration-link") ?>">
          <div class="banner-large">
            <span class="banner-large_headline wow fadeIn"><span class="underline">Арбитраж:</span></span>
            <p class="banner-large_text wow fadeIn"><span class="underline">Земельные </span><span class="underline">споры, и </span><span class="underline">споры по </span><span class="underline">договорам</span></p>
          </div>
        </a>
      </div>
      <div class="col-xl-3 col-md-12">
        <div class="row">
          <div class="col-xl-12 col-md-6 icons_v">
            <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-maintenance-link") ?>">
              <div class="icon-v wow slideInRight">
                <img class="icon_image" src="<?php echo get_template_directory_uri() ?>/img/icons/i_05.svg" alt="">
                <p class="icon_description">Правовое сопровождение деятельности компании</p>
              </div>
            </a>
          </div>
          <div class="col-xl-12 col-md-6">
            <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-examination-link") ?>">
              <div class="icon-v wow slideInRight">
                <img class="icon_image" src="<?php echo get_template_directory_uri() ?>/img/icons/i_08.svg" alt="">
                <p class="icon_description">Экспертиза, диагностика, оценка</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row protection">
      <div class="col-xl-7 col-md-12">
        <div class="row">
          <div class="col-xl-6 col-md-6">
            <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-project-lawyers-link") ?>">
              <div class="icon-h  wow slideInLeft">
                <img class="icon_image" src="<?php echo get_template_directory_uri() ?>/img/icons/i_07.svg" alt="">
                <p class="icon_description">Проектные юристы</p>
              </div>
            </a>
          </div>
          <div class="col-xl-6 col-md-6">
            <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-startups-link") ?>">
              <div class="icon-h  wow slideInLeft">
                <img class="icon_image" src="<?php echo get_template_directory_uri() ?>/img/icons/i_06.svg" alt="">
                <p class="icon_description">Стартапы и малый бизнес</p>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-xl-5 col-md-12">
        <a class="link-wrapper" href="<?php echo get_option("iks-mainpage-court-fas-link") ?>">
          <div class="banner-small">
            <p class="banner-small_text wow fadeIn">
              <span class="underline">Суды по </span>
              <span class="underline">госзакупкам </span>
              <span class="underline">и споры с </span>
              <span class="underline">ФАС</span>
            </p>
          </div>
        </a>
      </div>
    </div>
  </section>

  <section>
    <div class="about row">
      <div class="col-xl-5 col-md-4 col-xs-12">
<!--        <a href="#">-->
<!--          <img class="play wow fadeIn" src="--><?php //echo get_template_directory_uri() ?><!--/img/play.png" alt="">-->
<!--        </a>-->
      </div>
      <div class="col-xl-7 col-md-8 col-xs-12">
        <p class="wow slideInRight">Миссия ИКС — это выжимка смысла на три строки текста очень крупным заголовочным шрифтом</p>
        <span class="name wow slideInRight">Константин Константинопольский,</span>
        <span class="position wow slideInRight">управляющий партнер</span>
        <a class="link wow fadeIn" href="/about">О компании</a>
      </div>
    </div>
  </section>

  <?php include 'templates/reviews.php' ?>

  <section>
    <div class="row publications">
      <div class="col-lg-12 col-xl-6 publications_group">
        <h2 class="publications_headline wow fadeIn"><span class="line">Развиваем</span></br>
          <span class="line">российскую</span></br>
          <span class="line">науку</span></h2>
        <a class="link wow fadeIn" href="/media?filter=2">Все публикации</a>
      </div>
      <div class="col-lg-12 col-xl-6 publications_article">
        <?php
        for ($i = 1; $i <= 3; $i++):
	        $publicationID = get_option("iks-mainpage-post-id-" . $i);
            if ($publicationID != -1):
                ?>
        <div class="media wow slideInRight">
          <a href="<?php echo get_permalink($publicationID) ?>">
            <div class="image">
              <img width="120px" height="120px" src="<?php echo get_post_meta(get_post_meta($publicationID, "post-author-id", true), "employee-avatar-uri", true) ?>" alt="Generic placeholder image">
            </div>
            <div class="media-body">
              <p class="title"><?php echo get_the_title($publicationID) ?></p>
              <span class="name"><?php echo get_post_meta(get_post_meta($publicationID, "post-author-id", true), "employee-name", true) ?>,</span>
              <span class="position"><?php echo get_post_meta(get_post_meta($publicationID, "post-author-id", true), "employee-position", true) ?></span>
            </div>
          </a>
        </div>
        <?php endif; endfor; ?>
      </div>
    </div>
  </section>

  <?php
  $username     = get_option("iks-instagram");
  $client_id    = get_option('iks-instagram-client-id');
  $access_token = get_option('iks-instagram-access-token');
  $inst_loaded = true;
  set_error_handler(function() {global $inst_loaded; $inst_loaded = false;});
  $inst_data = file_get_contents("https://api.instagram.com/v1/users/self/media/recent?count=7&access_token=$access_token");
  restore_error_handler();
  if ($inst_loaded !== false):
  ?>
  <section class="social">
    <h2 class="social_headline wow fadeIn"><span class="line">Мы в соцсетях</span></h2>
    <a class="social_link link wow fadeIn" href="https://instagram.com/<?php echo get_option('iks-instagram') ?>">@<?php echo get_option('iks-instagram') ?></a>

    <div class="card-columns mobile-slider">
		<?php
		$json = json_decode($inst_data);
		foreach ($json->data as $image_data):
		?>
      <div class="card  wow fadeIn">
        <img class="card_img" src="<?php echo $image_data->images->standard_resolution->url ?>" alt="Image">
        <p class="card_text"><?php if ($image_data->caption) echo $image_data->caption->text ?></p>
      </div>
    <?php endforeach; ?>
    </div>
  </section>
  <?php endif; ?>

	<?php
	include 'templates/contacts-section.php';
	get_footer();
	?>
</div>


<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

  var options = {
    offset: 1080
  };

  var header = new Headhesive('.header', options);
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>


<script defer>
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.maskedinput.min.js"></script>
<script defer>
  $(function(){
    $("#number").mask("8(999) 999-9999");
  });
</script>

</body>
</html>