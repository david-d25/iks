<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
    <title>404</title>
  </head>
  <body>
    <section class="error">
        <div class="container">
            <div class="headline">
                <img src="<?php echo get_template_directory_uri() ?>/img/404.png" alt="">
                <h2><span class="line">Нет такой</span></br><span class="line">страницы</span></h2>
            </div>
        </div>
    </section>

    <?php
    include 'templates/contacts-section.php';
    get_footer();
    ?>

    <script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
    <script>
    new WOW().init();

    </script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>