<?php
add_option("iks-mainpage-post-id-1", "-1");
add_option("iks-mainpage-post-id-2", "-1");
add_option("iks-mainpage-post-id-3", "-1");
?>

<div class="wrap">
	<h2>
		<?php echo get_admin_page_title() ?>
	</h2>
	<form method="post" name="iks-mainpage-posts" action="options.php">
		<?php wp_nonce_field('update-options'); ?>

		<label class="posts-select-wr">
			Первая публикация:
			<select name="iks-mainpage-post-id-1">
				<?php
				$posts = get_posts(array("post_type" => "post", "numberposts" => "-1"));
				$names = array();
				foreach ($posts as $post)
					if (get_post_meta($post->ID, "post-author-id", true) != -1)
						$names[$post->ID] = get_the_title($post->ID);
				$names["-1"] = "Не выбрано";
				foreach ($names as $key => $value)
					echo "<option value='$key' " . (get_option("iks-mainpage-post-id-1") == $key ? "selected" : "") . ">$value</option>"
				?>
			</select>
		</label>

		<label class="posts-select-wr">
			Вторая публикация:
			<select name="iks-mainpage-post-id-2">
				<?php
				$posts = get_posts(array("post_type" => "post", "numberposts" => "-1"));
				$names = array();
				foreach ($posts as $post)
					if (get_post_meta($post->ID, "post-author-id", true) != -1)
						$names[$post->ID] = get_the_title($post->ID);
				$names["-1"] = "Не выбрано";
				foreach ($names as $key => $value)
					echo "<option value='$key' " . (get_option("iks-mainpage-post-id-2") == $key ? "selected" : "") . ">$value</option>"
				?>
			</select>
		</label>

		<label class="posts-select-wr">
			Третья публикация:
			<select name="iks-mainpage-post-id-3">
				<?php
				$posts = get_posts(array("post_type" => "post", "numberposts" => "-1"));
				$names = array();
				foreach ($posts as $post)
					if (get_post_meta($post->ID, "post-author-id", true) != -1)
						$names[$post->ID] = get_the_title($post->ID);
				$names["-1"] = "Не выбрано";
				foreach ($names as $key => $value)
					echo "<option value='$key' " . (get_option("iks-mainpage-post-id-3") == $key ? "selected" : "") . ">$value</option>"
				?>
			</select>
		</label>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="iks-mainpage-post-id-1,iks-mainpage-post-id-2,iks-mainpage-post-id-3" />
		<?php echo get_submit_button() ?>
	</form>
</div>

<style>
	.posts-select-wr {
		display: block;
		margin: 10px 0;
	}
</style>