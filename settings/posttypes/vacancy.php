<?php
add_action('add_meta_boxes', 'vacancyExtraFields', 1);
add_action('init', 'registerVacancyPostType' );
add_action('save_post', 'saveVacancyData' );

function vacancyExtraFields() {
	add_meta_box( 'vacancy-conditions-meta', 'Условия', 'vacancyConditions', 'vacancy', 'normal', 'high');
	add_meta_box( 'vacancy-responsibilities-meta', 'Обязанности', 'vacancyResponsibilities', 'vacancy', 'normal', 'high');
	add_meta_box( 'vacancy-requirements-meta', 'Требования', 'vacancyRequirements', 'vacancy', 'normal', 'high');
}

function saveVacancyData($postID) {
	update_post_meta($postID, "vacancy-conditions-content", $_POST["vacancy-conditions-content"]);
	update_post_meta($postID, "vacancy-responsibilities-content", $_POST["vacancy-responsibilities-content"]);
	update_post_meta($postID, "vacancy-requirements-content", $_POST["vacancy-requirements-content"]);
}

function vacancyConditions($post) {
	wp_editor(get_post_meta($post->ID, "vacancy-conditions-content", true), "vacancy-conditions-content");
}

function vacancyResponsibilities($post) {
	wp_editor(get_post_meta($post->ID, "vacancy-responsibilities-content", true), "vacancy-responsibilities-content");
}

function vacancyRequirements($post) {
	wp_editor(get_post_meta($post->ID, "vacancy-requirements-content", true), "vacancy-requirements-content");
}

function registerVacancyPostType() {
	$vacancyLabels = array(
		'name' => 'Вакансии',
		'singular_name' => 'Вакансию', // админ панель Добавить->Функцию
		'add_new' => 'Добавить вакансию',
		'add_new_item' => 'Добавить новую вакансию', // заголовок тега <title>
		'edit_item' => 'Редактировать вакансию',
		'new_item' => 'Новая вакансия',
		'all_items' => 'Все вакансии',
		'view_item' => 'Просмотр вакансии на сайте',
		'search_items' => 'Искать вакансии',
		'not_found' =>  'Вакансий не найдено.',
		'not_found_in_trash' => 'В корзине нет вакансий.',
		'menu_name' => 'Вакансии' // ссылка в меню в админке
	);
	$vacancyArgs = array(
		'labels' => $vacancyLabels,
		'public' => false,
		'show_ui' => true, // показывать интерфейс в админке
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'has_archive' => true,
//		'menu_icon' => get_stylesheet_directory_uri() .'/settings/icons/service_icon.png', // иконка в меню
		'menu_position' => 20, // порядок в меню
		'supports' => array('title')
	);
	register_post_type("vacancy", $vacancyArgs);
}