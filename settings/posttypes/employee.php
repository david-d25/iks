<?php
add_action("add_meta_boxes", "addEmployeeMetaBoxes");
add_action('init', 'registerEmployeePostType' );
add_action('save_post', 'saveEmployeeData' );

add_filter('wp_insert_post_data', 'changeEmployeeTitle');
function changeEmployeeTitle($data)
{
  if ($_POST["employee-name"])
	  $data['post_title'] = $_POST["employee-name"];
	return $data;
}

function addEmployeeMetaBoxes() {
	add_meta_box( 'employee-images', 'Фотографии', 'employeeImages', 'employee', 'side', 'high');
	add_meta_box( 'employee-info', 'О сотруднике', 'employeeInfo', 'employee', 'normal', 'high');
}

function saveEmployeeData($postID) {
  update_post_meta($postID, "employee-avatar-uri", $_POST["employee-avatar-uri"]);
  update_post_meta($postID, "employee-experience", $_POST["employee-experience"]);
  update_post_meta($postID, "employee-image-uri", $_POST["employee-image-uri"]);
  update_post_meta($postID, "employee-position", $_POST["employee-position"]);
	update_post_meta($postID, "employee-name", $_POST["employee-name"]);
	update_post_meta($postID, "employee-bio", $_POST["employee-bio"]);
}

function employeeImages($post) {
	?>
	<button id="employee-image">Выбрать изображение сотрудника</button>
	<button id="employee-avatar">Выбрать аватар сотрудника</button>
	<input type="hidden" id="employee-image-uri" name="employee-image-uri" value="<?php echo get_post_meta($post->ID, 'employee-image-uri', 1); ?>">
	<input type="hidden" id="employee-avatar-uri" name="employee-avatar-uri" value="<?php echo get_post_meta($post->ID, 'employee-avatar-uri', 1); ?>">

	<style>
		#employee-image,
		#employee-avatar {
			width: 100%;
			height: 300px;
			background-color: transparent;
			-webkit-background-size: cover;
			background-size: contain;
			background-position: center;
			background-repeat: no-repeat;
			cursor: pointer;
			border: dashed 4px rgba(0, 0, 0, .25);
		}
		#employee-avatar {
			margin-top: 10px;
			height: 200px;
		}
	</style>

	<script defer>
      document.getElementById("employee-image").style.backgroundImage = `url(${document.getElementById("employee-image-uri").value})`;
      document.getElementById("employee-avatar").style.backgroundImage = `url(${document.getElementById("employee-avatar-uri").value})`;

      if (document.getElementById("employee-image-uri").value !== "") document.getElementById("employee-image").innerHTML = "";
      $('#employee-image').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("employee-image-uri").value = image_url;
            document.getElementById("employee-image").style.backgroundImage = `url(${document.getElementById("employee-image-uri").value})`;
            document.getElementById("employee-image").innerHTML = "";
          });
      });
      if (document.getElementById("employee-avatar-uri").value !== "") document.getElementById("employee-avatar").innerHTML = "";
      $('#employee-avatar').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("employee-avatar-uri").value = image_url;
            document.getElementById("employee-avatar").style.backgroundImage = `url(${document.getElementById("employee-avatar-uri").value})`;
            document.getElementById("employee-avatar").innerHTML = "";
          });
      });
	</script>
	<?php
}

function employeeInfo($post) {
	?>
	<p>
		<label>
			Имя и фамилия
			<input type="text" name="employee-name" value="<?php echo get_post_meta($post->ID, 'employee-name', 1); ?>" style="width:100%" />
		</label>
	</p>
	<p>
		<label>
			Должность
			<input type="text" name="employee-position" value="<?php echo get_post_meta($post->ID, 'employee-position', 1); ?>" style="width:100%" />
		</label>
	</p>
  <p>
    <label>
      Опыт работы
      <input type="text" name="employee-experience" value="<?php echo get_post_meta($post->ID, 'employee-experience', 1); ?>" style="width:100%" />
    </label>
  </p>
	<p>
		<label>
			Биография
			<textarea type="text" name="employee-bio" style="width:100%"><?php echo get_post_meta($post->ID, 'employee-bio', 1); ?></textarea>
		</label>
	</p>
	<?php
}

function registerEmployeePostType() {
	$employeeLabels = array(
		'name' => 'Сотрудники',
		'singular_name' => 'Сотрудника', // админ панель Добавить->Функцию
		'add_new' => 'Добавить сотрудника',
		'add_new_item' => 'Добавить нового сотрудника', // заголовок тега <title>
		'edit_item' => 'Редактировать сотрудника',
		'new_item' => 'Новый сотрудник',
		'all_items' => 'Все сотрудники',
		'view_item' => 'Просмотр сотрудника на сайте',
		'search_items' => 'Искать сотрудников',
		'not_found' =>  'Сотрудников не найдено.',
		'not_found_in_trash' => 'В корзине нет сотрудников.',
		'menu_name' => 'Сотрудники' // ссылка в меню в админке
	);
	$employeeArgs = array(
		'labels' => $employeeLabels,
		'public' => false,
		'show_ui' => true, // показывать интерфейс в админке
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'has_archive' => true,
//		'menu_icon' => get_stylesheet_directory_uri() .'/settings/icons/service_icon.png', // иконка в меню
		'menu_position' => 20, // порядок в меню
		'supports' => array('thumbnail')
	);
	register_post_type("employee", $employeeArgs);
}