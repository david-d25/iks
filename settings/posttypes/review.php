<?php
add_action('add_meta_boxes', 'reviewExtraFields', 1);
add_action('init', 'registerReviewPostType' );
add_action('save_post', 'saveReviewData' );

function load_media_files() {
	wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_media_files' );

function reviewExtraFields() {
	add_meta_box( 'review-images', 'Изображение', 'reviewImages', 'review', 'normal', 'high');
}

function saveReviewData($postID) {
	update_post_meta($postID, "review-image-uri", $_POST["review-image-uri"]);
	update_post_meta($postID, "review-thumbnail-uri", $_POST["review-thumbnail-uri"]);
}

function reviewImages($post) {
	?>
	<button id="review-image">Крупное изображение отзыва</button>
	<button id="review-thumbnail">Превью отзыва</button>
	<input type="hidden" id="review-image-uri" name="review-image-uri" value="<?php echo get_post_meta($post->ID, 'review-image-uri', 1); ?>">
	<input type="hidden" id="review-thumbnail-uri" name="review-thumbnail-uri" value="<?php echo get_post_meta($post->ID, 'review-thumbnail-uri', 1); ?>">
	<style>
		#review-image,
		#review-thumbnail {
			width: 40%;
			min-width: 280px;
			max-width: 100%;
			height: 400px;
			background-color: transparent;
			-webkit-background-size: cover;
			background-size: contain;
			background-position: center;
			background-repeat: no-repeat;
			cursor: pointer;
			display: block;
			margin-bottom: 10px;
			border: dashed 4px rgba(0, 0, 0, .25);
		}
		#review-thumbnail {
			width: 20%;
			height: 200px;
		}
	</style>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script defer>
      document.getElementById("review-image").style.backgroundImage = `url(${document.getElementById("review-image-uri").value})`;
      document.getElementById("review-thumbnail").style.backgroundImage = `url(${document.getElementById("review-thumbnail-uri").value})`;

      if (document.getElementById("review-image-uri").value !== "") document.getElementById("review-image").innerHTML = "";
      if (document.getElementById("review-thumbnail-uri").value !== "") document.getElementById("review-thumbnail").innerHTML = "";
      $('#review-image').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("review-image-uri").value = image_url;
            document.getElementById("review-image").style.backgroundImage = `url(${document.getElementById("review-image-uri").value})`;
            document.getElementById("review-image").innerHTML = "";
          });
      });
      $('#review-thumbnail').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("review-thumbnail-uri").value = image_url;
            document.getElementById("review-thumbnail").style.backgroundImage = `url(${document.getElementById("review-thumbnail-uri").value})`;
            document.getElementById("review-thumbnail").innerHTML = "";
          });
      });
	</script>

	<?php
}

function registerReviewPostType() {
	$reviewLabels = array(
		'name' => 'Отзывы',
		'singular_name' => 'Отзыв', // админ панель Добавить->Функцию
		'add_new' => 'Добавить отзыв',
		'add_new_item' => 'Добавить новый отзыв', // заголовок тега <title>
		'edit_item' => 'Редактировать отзыв',
		'new_item' => 'Новый отзыв',
		'all_items' => 'Все отзывы',
		'view_item' => 'Просмотр отзыва на сайте',
		'search_items' => 'Искать отзывы',
		'not_found' =>  'Отзывов не найдено.',
		'not_found_in_trash' => 'В корзине нет отзывов.',
		'menu_name' => 'Отзывы' // ссылка в меню в админке
	);
	$reviewArgs = array(
		'labels' => $reviewLabels,
		'public' => false,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'show_ui' => true, // показывать интерфейс в админке
		'has_archive' => true,
//		'menu_icon' => get_stylesheet_directory_uri() .'/settings/icons/review_icon.png', // иконка в меню
		'menu_position' => 21, // порядок в меню
		'supports' => array('title', 'thumbnail')
	);
	register_post_type("review", $reviewArgs);
}