<?php
add_action('add_meta_boxes', 'postExtraFields');
add_action('save_post', 'savePostData');

function postExtraFields() {
	add_meta_box("post-additional", "Дополнительно", "postAdditional", "post", "normal");
}

function savePostData($postID) {
	update_post_meta($postID, "post-illustration-uri", $_POST["post-illustration-uri"]);
	update_post_meta($postID, "post-author-id", $_POST["post-author-id"]);
	update_post_meta($postID, "post-another-post", $_POST["post-another-post"]);
}

function postAdditional($post) {
	?>
	<button id="post-illustration" style="display: block;">Выбрать иллюстрацию</button>
	<input type="hidden" id="post-illustration-uri" name="post-illustration-uri" value="<?php echo get_post_meta($post->ID, 'post-illustration-uri', 1); ?>">

	<label style="display: block; margin: 10px 0;">
		Автор:
		<select id="post-author-id" name="post-author-id">
			<?php
			$authors = get_posts(array("post_type" => "employee", "numberposts" => "-1"));
			$names = array();
			foreach ($authors as $author)
				$names[$author->ID] = get_post_meta($author->ID, "employee-name", true);
			$names["-1"] = "Не выбран";
			foreach ($names as $key => $value)
				echo "<option value='$key' " . (get_post_meta($post->ID, "post-author-id", true) == $key ? "selected" : "") . ">$value</option>"
			?>
		</select>
	</label>

	<label>
		Врезка услуги:
		<select id="post-another-post" name="post-another-post">
			<?php
			$anotherPosts = get_posts(array("post_type" => "post", "numberposts" => "-1"));
			$names = array();
			foreach ($anotherPosts as $anotherPost)
				$names[$anotherPost->ID] = get_the_title($anotherPost->ID);
			$names["-1"] = "Не выбран";
			foreach ($names as $key => $value)
				echo "<option value='$key' " . (get_post_meta($post->ID, "post-another-post", true) == $key ? "selected" : "") . ">$value</option>"
			?>
		</select>
	</label>

	<style>
		#post-illustration {
			width: 50%;
			height: 75px;
			background-color: transparent;
			-webkit-background-size: cover;
			background-size: contain;
			background-position: center;
			background-repeat: no-repeat;
			cursor: pointer;
			border: dashed 4px rgba(0, 0, 0, .25);
		}
	</style>

	<script defer>
      document.getElementById("post-illustration").style.backgroundImage = `url(${document.getElementById("post-illustration-uri").value})`;

      if (document.getElementById("post-illustration-uri").value !== "") document.getElementById("post-illustration").innerHTML = "";
      $('#post-illustration').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("post-illustration-uri").value = image_url;
            document.getElementById("post-illustration").style.backgroundImage = `url(${document.getElementById("post-illustration-uri").value})`;
            document.getElementById("post-illustration").innerHTML = "";
          });
      });
	</script>
	<?php
}