<?php
add_action('add_meta_boxes', 'extraFields', 1);
add_action('init', 'registerPostType' );
add_action('save_post', 'saveData' );

function extraFields() {
  add_meta_box( 'service-protect', 'Зищащаем интересы клиентов', 'protect', 'service', 'normal', 'high');
  add_meta_box( 'service-cover', 'Обложка', 'serviceCover', 'service', 'normal', 'high');
  add_meta_box( 'service-teaser-block', 'Тизер-блок', 'teaserBlock', 'service', 'normal', 'high');
  add_meta_box( 'service-about', 'Об услуге', 'about', 'service', 'normal', 'high');
  add_meta_box( 'service-additional', 'Дополнительно', 'additional', 'service', 'normal', 'high');
  add_meta_box( 'service-footer-publications', 'Другие публикации', 'otherPublications', 'service', 'normal', 'high');
  add_meta_box( 'we-work-court', 'Как мы работаем - представление в суде', 'weWorkCourt', 'service', 'normal', 'high');
  add_meta_box( 'we-work-fas', 'Как мы работаем - представление в ФАС', 'weWorkFAS', 'service', 'normal', 'high');
}

function saveData($postID) {
  update_post_meta($postID, "work-court-leave-request-button", $_POST["work-court-leave-request-button"]);
  update_post_meta($postID, "work-FAS-leave-request-button", $_POST["work-FAS-leave-request-button"]);
  update_post_meta($postID, "service-teaser-description", $_POST["service-teaser-description"]);
  update_post_meta($postID, "service-practices-subtitle", $_POST["service-practices-subtitle"]);
  update_post_meta($postID, "work-court-contract-uri", $_POST["work-court-contract-uri"]);
  update_post_meta($postID, "service-about-content", $_POST["service-about-content"]);
  update_post_meta($postID, "teaser-block-icon-uri", $_POST["teaser-block-icon-uri"]);
  update_post_meta($postID, "work-FAS-contract-uri", $_POST["work-FAS-contract-uri"]);
  update_post_meta($postID, "online-payment-link", $_POST["online-payment-link"]);
  update_post_meta($postID, "service-post-id-1", $_POST["service-post-id-1"]);
  update_post_meta($postID, "service-post-id-2", $_POST["service-post-id-2"]);
  update_post_meta($postID, "service-post-id-3", $_POST["service-post-id-3"]);
  update_post_meta($postID, "service-post-id-4", $_POST["service-post-id-4"]);
  update_post_meta($postID, "illustration-uri", $_POST["illustration-uri"]);
  update_post_meta($postID, "service-subtitle", $_POST["service-subtitle"]);
  update_post_meta($postID, "service-section", $_POST["service-section"]);
  update_post_meta($postID, "service-price", $_POST["service-price"]);
  update_post_meta($postID, "protect-right", $_POST["protect-right"]);
  update_post_meta($postID, "service-time", $_POST["service-time"]);
  update_post_meta($postID, "teaser-title", $_POST["teaser-title"]);
  update_post_meta($postID, "protect-left", $_POST["protect-left"]);
  update_post_meta($postID, "work-court-1", $_POST["work-court-1"]);
  update_post_meta($postID, "work-court-2", $_POST["work-court-2"]);
  update_post_meta($postID, "work-court-3", $_POST["work-court-3"]);
  update_post_meta($postID, "work-court-4", $_POST["work-court-4"]);
  update_post_meta($postID, "work-FAS-1", $_POST["work-FAS-1"]);
  update_post_meta($postID, "work-FAS-2", $_POST["work-FAS-2"]);
  update_post_meta($postID, "work-FAS-3", $_POST["work-FAS-3"]);
  update_post_meta($postID, "work-FAS-4", $_POST["work-FAS-4"]);
}

function weWorkCourt($post) {
  ?>
  <label>
    <input
            type="checkbox"
            name="work-court-leave-request-button"
      <?php if (get_post_meta($post->ID, "work-court-leave-request-button", true)) echo "checked" ?>>
    Кнопка &laquo;Оставить заявку&raquo;
  </label>
  <button id="work-court-contract">Выбрать файл договора</button>
  <input type="hidden" id="work-court-contract-uri" name="work-court-contract-uri" value="<?php echo get_post_meta($post->ID, 'work-court-contract-uri', 1); ?>">

  <script defer>
    if (document.getElementById("work-court-contract-uri").value)
      document.getElementById("work-court-contract").innerText = document.getElementById("work-court-contract-uri").value.replace(/^.*[\\\/]/, '');

    $('#work-court-contract').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload file',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let fileURI = uploaded_image.toJSON().url;
          console.log(fileURI);
          document.getElementById("work-court-contract-uri").value = fileURI;
          document.getElementById("work-court-contract").innerText = document.getElementById("work-court-contract-uri").value.replace(/^.*[\\\/]/, '');
        });
    });
  </script>
  <?php
  for ($i = 1; $i <= 4; $i++) {
    echo "<h3>Суд: блок $i</h3>";
    wp_editor( get_post_meta( $post->ID, "work-court-$i", true ), "work-court-$i" );
  }
}

function weWorkFAS($post) {
  ?>
  <label>
    <input
            type="checkbox"
            name="work-FAS-leave-request-button"
      <?php if (get_post_meta($post->ID, "work-FAS-leave-request-button", true)) echo "checked" ?>>
    Кнопка &laquo;Оставить заявку&raquo;
  </label>
  <button id="work-FAS-contract">Выбрать файл договора</button>
  <input type="hidden" id="work-FAS-contract-uri" name="work-FAS-contract-uri" value="<?php echo get_post_meta($post->ID, 'work-FAS-contract-uri', 1); ?>">

  <script defer>
    if (document.getElementById("work-FAS-contract-uri").value)
      document.getElementById("work-FAS-contract").innerText = document.getElementById("work-FAS-contract-uri").value.replace(/^.*[\\\/]/, '');

    $('#work-FAS-contract').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload file',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let fileURI = uploaded_image.toJSON().url;
          console.log(fileURI);
          document.getElementById("work-FAS-contract-uri").value = fileURI;
          document.getElementById("work-FAS-contract").innerText = document.getElementById("work-FAS-contract-uri").value.replace(/^.*[\\\/]/, '');
        });
    });
  </script>
  <?php
  for ($i = 1; $i <= 4; $i++) {
    echo "<h3>ФАС: блок $i</h3>";
    wp_editor( get_post_meta( $post->ID, "work-FAS-$i", true ), "work-FAS-$i" );
  }
}

function registerPostType() {
  $serviceLabels = array(
    'name' => 'Услуги',
    'singular_name' => 'Услугу', // админ панель Добавить->Функцию
    'add_new' => 'Добавить услугу',
    'add_new_item' => 'Добавить новую услугу', // заголовок тега <title>
    'edit_item' => 'Редактировать услугу',
    'new_item' => 'Новая услуга',
    'all_items' => 'Все услуги',
    'view_item' => 'Просмотр услуги на сайте',
    'search_items' => 'Искать услуги',
    'not_found' =>  'Услуг не найдено.',
    'not_found_in_trash' => 'В корзине нет услуг.',
    'menu_name' => 'Услуги' // ссылка в меню в админке
  );
  $serviceArgs = array(
    'labels' => $serviceLabels,
    'public' => true,
    'show_ui' => true,
    'has_archive' => true,
//		'menu_icon' => get_stylesheet_directory_uri() .'/settings/icons/service_icon.png', // иконка в меню
    'menu_position' => 20,
    'supports' => array('title')
  );
  register_post_type("service", $serviceArgs);
}

function otherPublications($post) {
  ?>
  <style>
    .posts-select-wr {
      display: block;
    }
  </style>
  <?php
  foreach (range(1, 4) as $i): ?>
    <label class="posts-select-wr">
      Публикация <?php echo $i ?>:
      <select name="service-post-id-<?php echo $i ?>">
        <?php
        $posts = get_posts(array("post_type" => "post", "numberposts" => "-1"));
        $names = array();
        foreach ($posts as $otherPost)
          if (get_post_meta($otherPost->ID, "post-author-id", true) != -1)
            $names[$otherPost->ID] = get_the_title($otherPost->ID);
        $names["-1"] = "Не выбрано";
        foreach ($names as $key => $value)
          echo "<option value='$key' " . (get_post_meta($post->ID, "service-post-id-" . $i, true) == $key ? "selected" : "") . ">$value</option>"
        ?>
      </select>
    </label>
  <?php endforeach;
}

function serviceCover($post) {
  ?>
  <button id="illustration">Выбрать иллюстрацию услуги</button>
  <input type="hidden" id="illustration-uri" name="illustration-uri" value="<?php echo get_post_meta($post->ID, 'illustration-uri', 1); ?>">

  <p>
    <label>
      Подзаголовок
      <input type="text" name="service-subtitle" value="<?php echo get_post_meta($post->ID, 'service-subtitle', 1); ?>" style="width:100%" />
    </label>
  </p>

  <style>
    #illustration,
    #work-FAS-contract,
    #work-court-contract {
      width: 100%;
      height: 200px;
      background-color: transparent;
      -webkit-background-size: cover;
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
      cursor: pointer;
      border: dashed 4px rgba(0, 0, 0, .25);
    }

    #work-FAS-contract,
    #work-court-contract {
      display: block;
      margin: 10px;
      width: 250px;
      height: 100px;
    }
  </style>

  <script defer>
    document.getElementById("illustration").style.backgroundImage = `url(${document.getElementById("illustration-uri").value})`;

    if (document.getElementById("illustration-uri").value !== "") document.getElementById("illustration").innerHTML = "";
    $('#illustration').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload Image',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let image_url = uploaded_image.toJSON().url;
          console.log(image_url);
          document.getElementById("illustration-uri").value = image_url;
          document.getElementById("illustration").style.backgroundImage = `url(${document.getElementById("illustration-uri").value})`;
          document.getElementById("illustration").innerHTML = "";
        });
    });
  </script>
  <?php
}

function about($post) {
  ?><h3>Блок &laquo;Об услуге&raquo;</h3><?php
  wp_editor(get_post_meta($post->ID, "service-about-content", true), "service-about-content");
}

function additional($post) {
  ?>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

  <p>
    <label>
      Ссылка на оплату онлайн
      <input type="text" name="online-payment-link" value="<?php echo get_post_meta($post->ID, 'online-payment-link', 1); ?>" style="width:100%" />
    </label>
  </p>

  <p>
    <label>
      Срок
      <input type="text" name="service-time" value="<?php echo get_post_meta($post->ID, 'service-time', 1); ?>" />
    </label>
  </p>

  <p>
    <label>
      Цена
      <input type="number" min="0" name="service-price" value="<?php echo get_post_meta($post->ID, 'service-price', 1); ?>" />
      &#8381;
    </label>
  </p>

  <p>
    <label>
      Подзаголовок &laquo;Примеры из практики&raquo;
      <input type="text" name="service-practices-subtitle" value="<?php echo get_post_meta($post->ID, 'service-practices-subtitle', 1); ?>" style="width:50%;min-width: 260px"  />
    </label>
  </p>

  <p>
    <label>
      Услуга относится к разделу
      <select name="service-section">
        <option value="0" <?php if (get_post_meta($post->ID, 'service-section', true) == 0) echo "selected"; ?>>Суды и споры</option>
        <option value="1" <?php if (get_post_meta($post->ID, 'service-section', true) == 1) echo "selected"; ?>>Правовая экспертиза</option>
        <option value="2" <?php if (get_post_meta($post->ID, 'service-section', true) == 2) echo "selected"; ?>>Поддержка стартапов</option>
        <option value="3" <?php if (get_post_meta($post->ID, 'service-section', true) == 3) echo "selected"; ?>>Абонентское обслуживание</option>
      </select>
    </label>
  </p>
  <?php
}

function protect($post) {
  ?><h3>Левая колонка</h3><?php
  wp_editor(get_post_meta($post->ID, "protect-left", true), "protect-left");
  ?><h3>Правая колонка</h3><?php
  wp_editor(get_post_meta($post->ID, "protect-right", true), "protect-right");
}

function teaserBlock($post){
  ?>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

  <button id="teaser-block-icon">Выбрать иконку тизер-блока</button>
  <input type="hidden" id="teaser-block-icon-uri" name="teaser-block-icon-uri" value="<?php echo get_post_meta($post->ID, 'teaser-block-icon-uri', 1); ?>">
  <style>
    #teaser-block-icon {
      width: 25%;
      min-width: 250px;
      height: 200px;
      background-color: transparent;
      -webkit-background-size: cover;
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
      cursor: pointer;
      border: dashed 4px rgba(0, 0, 0, .25);
    }
  </style>

  <p>
    <label>
      Лид-абзац
      <input type="text" name="teaser-title" value="<?php echo get_post_meta($post->ID, 'teaser-title', 1); ?>" style="width:100%" />
    </label>
  </p>

  <h3>Описание для тизер-блока</h3><?php
  wp_editor(get_post_meta($post->ID, "service-teaser-description", true), "service-teaser-description");
  ?>

  <script defer>
    document.getElementById("teaser-block-icon").style.backgroundImage = `url(${document.getElementById("teaser-block-icon-uri").value})`;

    if (document.getElementById("teaser-block-icon-uri").value !== "") document.getElementById("teaser-block-icon").innerHTML = "";
    $('#teaser-block-icon').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload Image',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let image_url = uploaded_image.toJSON().url;
          console.log(image_url);
          document.getElementById("teaser-block-icon-uri").value = image_url;
          document.getElementById("teaser-block-icon").style.backgroundImage = `url(${document.getElementById("teaser-block-icon-uri").value})`;
          document.getElementById("teaser-block-icon").innerHTML = "";
        });
    });
  </script>
  <?php
}