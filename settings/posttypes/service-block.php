<?php
add_action('add_meta_boxes', 'serviceBlockExtraBoxes', 1);
add_action('init', 'registerServiceBlockPostType' );
add_action('save_post', 'saveServiceBlockData' );

function serviceBlockExtraBoxes() {
	add_meta_box("service-block-files", "Файлы блока", "serviceBlockFiles", "service-block", "normal");
	add_meta_box("service-block-services", "Услуги, входящие в блок", "serviceBlockServices", "service-block", "normal");
}

function saveServiceBlockData($postID) {
  update_post_meta($postID, "service-block-print-version-uri", $_POST["service-block-print-version-uri"]);
  update_post_meta($postID, "service-block-pdf-file-uri", $_POST["service-block-pdf-file-uri"]);
	update_post_meta($postID, "service-block-services-data", $_POST["service-block-services-data"]);
	update_post_meta($postID, "service-block-section", $_POST["service-block-section"]);
}

function serviceBlockFiles($post) {
	?>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

	<h3>Файлы блока услуг</h3>

  <h4>PDF-файл для скачивания</h4>
  <button id="service-block-pdf-file"><span style="color: grey">Файл не выбран</span></button>
  <h4>Версия для печати</h4>
  <button id="service-block-print-version"><span style="color: grey">Файл не выбран</span></button>

	<input type="hidden" id="service-block-pdf-file-uri" name="service-block-pdf-file-uri" value="<?php echo get_post_meta($post->ID, 'service-block-pdf-file-uri', 1); ?>">
	<input type="hidden" id="service-block-print-version-uri" name="service-block-print-version-uri" value="<?php echo get_post_meta($post->ID, 'service-block-print-version-uri', 1); ?>">

	<style>
		#service-block-pdf-file,
		#service-block-print-version {
			width: 20%;
			min-width: 250px;
			height: 100px;
			background-color: transparent;
			-webkit-background-size: cover;
			background-size: contain;
			background-position: center;
			background-repeat: no-repeat;
			cursor: pointer;
			border: dashed 4px rgb(197, 118, 64);
		}
	</style>

	<script defer>
		if (document.getElementById("service-block-pdf-file-uri").value) {
      document.getElementById("service-block-pdf-file").innerText = document.getElementById("service-block-pdf-file-uri").value.replace(/^.*[\\\/]/, '');
      document.getElementById("service-block-pdf-file").style.borderColor = 'rgb(66, 244, 110)';
    }

    if (document.getElementById("service-block-print-version-uri").value) {
      document.getElementById("service-block-print-version").innerText = document.getElementById("service-block-print-version-uri").value.replace(/^.*[\\\/]/, '');
      document.getElementById("service-block-print-version").style.borderColor = 'rgb(66, 244, 110)';
    }

    $('#service-block-pdf-file').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload File',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let fileUrl = uploaded_image.toJSON().url;
          console.log(fileUrl);
          document.getElementById("service-block-pdf-file-uri").value = fileUrl;
          document.getElementById("service-block-pdf-file").innerText = document.getElementById("service-block-pdf-file-uri").value.replace(/^.*[\\\/]/, '');
          document.getElementById("service-block-pdf-file").style.borderColor = 'rgb(66, 244, 110)';
        });
    });

    $('#service-block-print-version').click(function(e) {
      e.preventDefault();
      let image = wp.media({
        title: 'Upload File',
        multiple: false
      }).open()
        .on('select', function () {
          let uploaded_image = image.state().get('selection').first();
          console.log(uploaded_image);
          let fileUrl = uploaded_image.toJSON().url;
          console.log(fileUrl);
          document.getElementById("service-block-print-version-uri").value = fileUrl;
          document.getElementById("service-block-print-version").innerText = document.getElementById("service-block-print-version-uri").value.replace(/^.*[\\\/]/, '');
          document.getElementById("service-block-print-version").style.borderColor = 'rgb(66, 244, 110)';
        });
    });
	</script>
	<?php
}

function serviceBlockServices($post) {
  $selected_services = json_decode(get_post_meta($post->ID, "service-block-services-data", true));
  $services_list = get_posts(array("post_type" => "service", "numberposts" => "-1"));
  $names = array();
  foreach ($services_list as $service)
    $names[$service->ID] = get_the_title($service->ID);
  $names["-1"] = "Не выбран";
  ?>
    <p>
      <label>
        Услуга относится к разделу
        <select name="service-block-section">
          <option value="0" <?php if (get_post_meta($post->ID, 'service-block-section', true) == 0) echo "selected"; ?>>Суды и споры</option>
          <option value="1" <?php if (get_post_meta($post->ID, 'service-block-section', true) == 1) echo "selected"; ?>>Правовая экспертиза</option>
          <option value="2" <?php if (get_post_meta($post->ID, 'service-block-section', true) == 2) echo "selected"; ?>>Поддержка стартапов</option>
          <option value="3" <?php if (get_post_meta($post->ID, 'service-block-section', true) == 3) echo "selected"; ?>>Абонентское обслуживание</option>
        </select>
      </label>
    </p>
    <div id="service-block__services-list"></div>
    <input type="button" id="service-block__add-service-button" value="Добавить услугу в этот блок" style="margin: 10px;">
    <input type="hidden" name="service-block-services-data" id="service-block-services-data" value="<?php echo get_post_meta($post->ID, "service-block-services", true)?>">
    <script>
      let names = {};
      let selectedIDs = [];

      <?php if (json_encode($names)): ?>
      names = <?php echo json_encode($names) ?>;
      <?php endif ?>

      <?php if (json_encode($selected_services)): ?>
      selectedIDs = <?php echo json_encode($selected_services) ?>;
      <?php endif ?>

      let dataTarget = document.getElementById("service-block-services-data");

      let button = document.getElementById("service-block__add-service-button");
      for (let id in selectedIDs)
        generateSelect(names, selectedIDs[id]);
      servicesListChanged();

      button.onclick = function (e) {
        e.preventDefault();
        generateSelect(names, "-1");
        servicesListChanged();
      };

      function servicesListChanged() {
          let selects = document.querySelectorAll(".service-block__select");
          let ids = [];
          for (let i = 0; i < selects.length; i++) {
            let id = selects[i].options[selects[i].selectedIndex].value;
            if (+id !== -1)
              ids.push(+id);
          }
          dataTarget.value = JSON.stringify(ids);
      }

      function generateSelect(data, selected) {
        let listTarget = document.getElementById("service-block__services-list");
        let element = document.createElement("select");
        element.onchange = servicesListChanged;
        element.classList.add("service-block__select");
        for (let id in data)
          element.innerHTML += `<option value="${id}" ${+id === +selected ? "selected" : ""}>${data[id]}</option>`;
        listTarget.appendChild(element);
      }
    </script>
  <style>
    select {
      display: block;
    }
  </style>
  <?php
}

function registerServiceBlockPostType() {
	$serviceBlockLabels = array(
		'name' => 'Блок услуг',
		'singular_name' => 'Блок услуг',
		'add_new' => 'Добавить блок услуг',
		'add_new_item' => 'Добавить новый блок услуг',
		'edit_item' => 'Редактировать блок услуг',
		'new_item' => 'Новый блок услуг',
		'all_items' => 'Все блоки услуг',
		'view_item' => 'Просмотр блока услуги на сайте',
		'search_items' => 'Искать блоки услуг',
		'not_found' =>  'Блоков услуг не найдено.',
		'not_found_in_trash' => 'В корзине нет блоков услуг.',
		'menu_name' => 'Блоки услуг'
	);
	$serviceBlockArgs = array(
		'labels' => $serviceBlockLabels,
		'public' => false,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'has_archive' => true,
//		'menu_icon' => get_stylesheet_directory_uri() .'/settings/icons/service_icon.png',
		'menu_position' => 20,
		'supports' => array('title')
	);
	register_post_type("service-block", $serviceBlockArgs);
}