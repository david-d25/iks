<?php
	add_option("iks-services-data", "[]");
?>
<div class="wrap">
	<h2>
		<?php echo get_admin_page_title() ?>
	</h2>
  <h3>Блоки</h3>
  <div class="blocks"></div>
  <input type="button" class="button add-block-btn" onclick="addBlock()" value="Добавить блок">
  <form method="post" name="iks-services" action="options.php">
		<?php wp_nonce_field('update-options'); ?>
		<input type="hidden" name="iks-services-data" id="settings-src" value='<?php echo get_option('iks-services-data') ?>'>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="iks-services-data" />
		<?php echo get_submit_button() ?>
	</form>
</div>

<script id="block-template" type="text/x-handlebars-template">
  <div class="blocks__block">
    <a class="control-button-inline up blue" onclick="replaceBlocks({{ blockIndex }}, {{ blockIndex }}-1)">Выше</a>
    <a class="control-button-inline down blue" onclick="replaceBlocks({{ blockIndex }}, {{ blockIndex }}+1)">Ниже</a>
    <a class="control-button-inline remove red" onclick="removeBlock({{ blockIndex }})">Удалить</a>
    <input type="text" value="{{ name }}" onchange="settings[{{blockIndex}}].name = this.value; putSettings(settings)" placeholder="Название"/>
    <h3>Тизеры</h3>
    <div class="teasers">{{{ teasers }}}</div>
    <input type="button" class="button add-teaser-btn" onclick="addTeaser({{ blockIndex }})" value="Добавить тизер">
  </div>
</script>

<script id="teaser-template" type="text/x-handlebars-template">
  <div class="teasers__teaser">
    <a class="control-button-inline up blue" onclick="replaceTeasers({{blockIndex}}, {{teaserIndex}}, {{teaserIndex}}-1)">Выше</a>
    <a class="control-button-inline down blue" onclick="replaceTeasers({{blockIndex}}, {{teaserIndex}}, {{teaserIndex}}+1)">Ниже</a>
    <a class="control-button-inline remove red" onclick="removeTeaser({{ blockIndex }}, {{teaserIndex}})">Удалить</a>
    <input type="text" value="{{ title }}" placeholder="Заголовок" onchange="settings[{{blockIndex}}].teasers[{{teaserIndex}}].title = this.value; putSettings(settings)"/>
    <input type="text" value="{{ illustration }}" placeholder="Ссылка на иллюстрацию" onchange="settings[{{blockIndex}}].teasers[{{teaserIndex}}].illustration = this.value; putSettings(settings)"/>
    <textarea placeholder="Описание" onchange="settings[{{blockIndex}}].teasers[{{teaserIndex}}].description = this.value; putSettings(settings)">{{ description }}</textarea>
  </div>
</script>

<style>
	.blocks {
		width: 100%;
    max-width: 600px;
    margin-bottom: 20px;
	}
  .blocks input,
  .blocks textarea {
    margin: 20px 20px 0;
    width: 90%;
  }

  .blocks__block {
    min-height: 20px;
    background: #ddd;
    padding: 20px;
    margin-top: 10px;
  }
  .blocks__block:nth-child(2n) {
    background: #ccc;
  }
  .blocks__block h3 {
    margin-left: 20px;
    margin-bottom: 0;
  }
  .blocks__block:first-child > .control-button-inline.up,
  .blocks__block:last-child > .control-button-inline.down,
  .teasers__teaser:first-child .control-button-inline.up,
  .teasers__teaser:last-child .control-button-inline.down {
    display: none;
  }

  .teasers {
    padding: 20px;
  }
  .teasers__teaser {
    background: #b0b0b0;
    padding: 20px;
    margin: 10px;
  }
  .teasers__teaser:nth-child(2n) {
    background: #b8b8b8;
  }
  .add-teaser-btn {
    display: block !important;
    margin: auto !important;
  }
  .control-button-inline {
    cursor: pointer;
    margin-left: 20px;
  }
  .control-button-inline.blue {
    color: rgb(50, 50, 255);
    border-bottom: dotted rgb(50, 50, 255) 1px;
  }

  .control-button-inline.red {
    color: #C00;
    border-bottom: dotted #C00 1px;
  }
</style>

<script src="<?php echo get_template_directory_uri() ?>/js/handlebars.js"></script>
<script defer>
	let settings = getSettings();
	updateHTML(settings);

	function updateHTML(settings) {
	  let target = document.querySelector(".blocks");
	  target.innerHTML = "";

    let blockSource = document.getElementById("block-template").innerHTML;
    let teaserSource = document.getElementById("teaser-template").innerHTML;

    let blockTemplate = Handlebars.compile(blockSource);
    let teaserTemplate = Handlebars.compile(teaserSource);

    for (let block in settings) {
      let teasersHTML = "";

      for (let teaser in settings[block || {}].teasers)
        teasersHTML += teaserTemplate({
          title: settings[block].teasers[teaser].title,
          illustration: settings[block].teasers[teaser].illustration,
          description: settings[block].teasers[teaser].description,
          blockIndex: block,
          teaserIndex: teaser
        });

      target.innerHTML += blockTemplate({
        name: settings[block || {}].name,
        blockIndex: block || 0,
        teasers: teasersHTML
      });
    }
  }
  
  function addBlock() {
    settings.push({name: "", teasers: []});
    putSettings(settings);
    updateHTML(settings);
  }

  function removeBlock(blockIndex) {
    settings.splice(blockIndex, 1);
    putSettings(settings);
    updateHTML(settings);
  }

  function removeTeaser(blockIndex, teaserIndex) {
	  settings[blockIndex].teasers.splice(teaserIndex, 1);
    putSettings(settings);
    updateHTML(settings);
  }

  function replaceBlocks(b1, b2) {
	  if (b1 < 0 || b2 < 0 || b1 >= settings.length || b2 >= settings.length) return;
    let buffer = settings[b1];
    settings[b1] = settings[b2];
    settings[b2] = buffer;
    putSettings(settings);
    updateHTML(settings);
  }

  function replaceTeasers(blockIndex, t1, t2) {
    if (
      blockIndex < 0 ||
      blockIndex >= settings.length ||
      t1 < 0 || t2 < 0 ||
      t1 >= settings[blockIndex].teasers.length ||
      t2 >= settings[blockIndex].teasers.length
    ) return;

    let buffer = settings[blockIndex].teasers[t1];
    settings[blockIndex].teasers[t1] = settings[blockIndex].teasers[t2];
    settings[blockIndex].teasers[t2] = buffer;
    putSettings(settings);
    updateHTML(settings);
  }
  
  function addTeaser(blockIndex) {
    settings[blockIndex].teasers.push({
      title: "Новый тизер",
      description: "Это мой новый тизер!",
      illustration: "https://source.unsplash.com/random?r="+Math.random()
    });
    putSettings(settings);
    updateHTML(settings);
  }

  function putSettings(settings) {
    document.getElementById('settings-src').value = JSON.stringify(settings);
  }
  function getSettings() {
    return JSON.parse(document.getElementById('settings-src').value || "[]");
  }
</script>