<?php
add_option("iks-mainpage-slider-data", "[]");
add_option("iks-mainpage-slider-bg-uri", "");
?>
<div class="wrap">
  <form method="post" name="iks-mainpage-slider" action="options.php">
    <h2>
      <?php echo get_admin_page_title() ?>
    </h2>
    <h3>Фон слайдера</h3>
    <button id="iks-mainpage-slider-bg">Выберите фон слайдера</button>
    <input type="hidden" id="iks-mainpage-slider-bg-uri" name="iks-mainpage-slider-bg-uri" value="<?php echo get_option('iks-mainpage-slider-bg-uri'); ?>">
    <style>
      #iks-mainpage-slider-bg,
      #iks-mainpage-slider-bg {
        width: 40%;
        min-width: 280px;
        max-width: 100%;
        height: 400px;
        background-color: transparent;
        -webkit-background-size: cover;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        cursor: pointer;
        display: block;
        margin-bottom: 10px;
        border: dashed 4px rgba(0, 0, 0, .25);
      }
      #iks-mainpage-slider-bg {
        width: 20%;
        height: 200px;
      }
    </style>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script defer>
      document.getElementById("iks-mainpage-slider-bg").style.backgroundImage = `url(${document.getElementById("iks-mainpage-slider-bg-uri").value})`;

      if (document.getElementById("iks-mainpage-slider-bg-uri").value !== "") document.getElementById("iks-mainpage-slider-bg").innerHTML = "";
      $('#iks-mainpage-slider-bg').click(function(e) {
        e.preventDefault();
        let image = wp.media({
          title: 'Upload Image',
          multiple: false
        }).open()
          .on('select', function () {
            let uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            let image_url = uploaded_image.toJSON().url;
            console.log(image_url);
            document.getElementById("iks-mainpage-slider-bg-uri").value = image_url;
            document.getElementById("iks-mainpage-slider-bg").style.backgroundImage = `url(${document.getElementById("iks-mainpage-slider-bg-uri").value})`;
            document.getElementById("iks-mainpage-slider-bg").innerHTML = "";
          });
      });
    </script>

    <h3>Слайды</h3>
    <div class="slides"></div>
    <input type="button" class="button add-slide-btn" onclick="addSlide()" value="Добавить слайд">
	  <?php wp_nonce_field('update-options'); ?>
    <input type="hidden" name="iks-mainpage-slider-data" id="settings-src" value='<?php echo get_option('iks-mainpage-slider-data') ?>'>
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="iks-mainpage-slider-data,iks-mainpage-slider-bg-uri" />
	  <?php echo get_submit_button() ?>
  </form>
</div>

<script id="slide-template" type="text/x-handlebars-template">
  <div class="slides__slide">
    <a class="control-button-inline up blue" onclick="replaceSlides({{ slideIndex }}, {{ slideIndex }}-1)">Выше</a>
    <a class="control-button-inline down blue" onclick="replaceSlides({{ slideIndex }}, {{ slideIndex }}+1)">Ниже</a>
    <a class="control-button-inline remove red" onclick="removeSlide({{ slideIndex }})">Удалить</a>
    <input type="text" value="{{ title }}" onchange="settings[{{slideIndex}}].title = this.value; putSettings(settings)" placeholder="Заголовок"/>
    <input type="text" value="{{ illustration }}" onchange="settings[{{slideIndex}}].illustration = this.value; putSettings(settings)" placeholder="Иллюстрация"/>
    <input type="text" value="{{ text }}" onchange="settings[{{slideIndex}}].text = this.value; putSettings(settings)" placeholder="Текст"/>
    <input type="text" value="{{ buttonCaption }}" onchange="settings[{{slideIndex}}].buttonCaption = this.value; putSettings(settings)" placeholder="Название кнопки"/>
    <input type="text" value="{{ buttonLink }}" onchange="settings[{{slideIndex}}].buttonLink = this.value; putSettings(settings)" placeholder="Ссылка кнопки"/>
  </div>
</script>

<style>
  .slides {
    width: 100%;
    max-width: 600px;
    margin-bottom: 20px;
  }
  .slides input,
  .slides textarea {
    margin: 20px 20px 0;
    width: 90%;
  }

  .slides__slide {
    min-height: 20px;
    background: #ddd;
    padding: 20px;
    margin-top: 10px;
  }
  .slides__slide:nth-child(2n) {
    background: #ccc;
  }
  .slides__slide h3 {
    margin-left: 20px;
    margin-bottom: 0;
  }
  .slides__slide:first-child > .control-button-inline.up,
  .slides__slide:last-child > .control-button-inline.down {
    display: none;
  }
  .control-button-inline {
    cursor: pointer;
    margin-left: 20px;
  }
  .control-button-inline.blue {
    color: rgb(50, 50, 255);
    border-bottom: dotted rgb(50, 50, 255) 1px;
  }

  .control-button-inline.red {
    color: #C00;
    border-bottom: dotted #C00 1px;
  }
</style>

<script src="<?php echo get_template_directory_uri() ?>/js/handlebars.js"></script>
<script defer>
  let settings = getSettings();
  updateHTML(settings);

  function updateHTML(settings) {
    let target = document.querySelector(".slides");
    target.innerHTML = "";

    let slideSource = document.getElementById("slide-template").innerHTML;

    let slideTemplate = Handlebars.compile(slideSource);

    for (let block in settings) {
      target.innerHTML += slideTemplate({
        title: settings[block].title,
        illustration: settings[block].illustration,
        text: settings[block].text,
        buttonCaption: settings[block].buttonCaption,
        buttonLink: settings[block].buttonLink,
        slideIndex: block || 0
      });
    }
  }

  function addSlide() {
    settings.push({
      title: "Новый слайд",
      illustration: "https://source.unsplash.com/random?r="+Math.random(),
      text: "Текст нового слайда",
      buttonCaption: "Нажми на меня",
      buttonLink: "example.com"
    });

    putSettings(settings);
    updateHTML(settings);
  }

  function removeSlide(blockIndex) {
    settings.splice(blockIndex, 1);
    putSettings(settings);
    updateHTML(settings);
  }


  function replaceSlides(b1, b2) {
    if (b1 < 0 || b2 < 0 || b1 >= settings.length || b2 >= settings.length) return;
    let buffer = settings[b1];
    settings[b1] = settings[b2];
    settings[b2] = buffer;
    putSettings(settings);
    updateHTML(settings);
  }

  function putSettings(settings) {
    document.getElementById('settings-src').value = JSON.stringify(settings);
  }
  function getSettings() {
    return JSON.parse(document.getElementById('settings-src').value || "[]");
  }
</script>