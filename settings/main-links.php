<?php
add_option("iks-mainpage-arbitration-link", "");
add_option("iks-mainpage-maintenance-link", "");
add_option("iks-mainpage-examination-link", "");

add_option("iks-mainpage-project-lawyers-link", "");
add_option("iks-mainpage-startups-link", "");
add_option("iks-mainpage-court-fas-link", "");
?>
<div class="wrap">
  <h2>
    <?php echo get_admin_page_title() ?>
  </h2>
  <form method="post" action="options.php">
    <?php wp_nonce_field('update-options'); ?>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Арбитраж: земельные споры</th>
        <td>
          <input type="text" name="iks-mainpage-arbitration-link" value="<?php echo get_option('iks-mainpage-arbitration-link'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Правовое сопровождение деятельности компании</th>
        <td>
          <input type="text" name="iks-mainpage-maintenance-link" value="<?php echo get_option('iks-mainpage-maintenance-link'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Экспертиза, диагностика, оценка</th>
        <td>
          <input type="text" name="iks-mainpage-examination-link" value="<?php echo get_option('iks-mainpage-examination-link'); ?>" />
        </td>
      </tr>
    </table>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Проектные юристы</th>
        <td>
          <input type="text" name="iks-mainpage-project-lawyers-link" value="<?php echo get_option('iks-mainpage-project-lawyers-link'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Стартапы и малый бизнес</th>
        <td>
          <input type="text" name="iks-mainpage-startups-link" value="<?php echo get_option('iks-mainpage-startups-link'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Суды по госзакупкам</th>
        <td>
          <input type="text" name="iks-mainpage-court-fas-link" value="<?php echo get_option('iks-mainpage-court-fas-link'); ?>" />
        </td>
      </tr>
    </table>

    <input type="hidden" name="action" value="update" />

    <input type="hidden" name="page_options" value="
      iks-mainpage-arbitration-link,
      iks-mainpage-maintenance-link,
      iks-mainpage-examination-link,
      iks-mainpage-project-lawyers-link,
      iks-mainpage-startups-link,
      iks-mainpage-court-fas-link
    "/>

    <?php echo get_submit_button() ?>
  </form>
</div>

<style>
  tr > th,
  tr > td {
    padding-top: 8px !important;
  }
</style>