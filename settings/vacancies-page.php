<?php
add_option("iks-vacancies-work-is", "");
?>

<div class="wrap">
	<h2>
		<?php echo get_admin_page_title() ?>
	</h2>
	<h3>Работа в ИКС - это:</h3>
	<form method="post" name="iks-service-slider" action="options.php">
		<?php
		wp_nonce_field('update-options');
		wp_editor(get_option("iks-vacancies-work-is"), "iks-vacancies-work-is");
		?>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="iks-vacancies-work-is" />
		<?php echo get_submit_button() ?>
	</form>
</div>
