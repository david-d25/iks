<?php
add_option("iks-service-slider-data", "[]");
?>
<div class="wrap">
	<h2>
		<?php echo get_admin_page_title() ?>
	</h2>
	<h3>Слайды</h3>
	<div class="slides"></div>
	<input type="button" class="button add-slide-btn" onclick="addSlide()" value="Добавить слайд">
	<form method="post" name="iks-service-slider" action="options.php">
		<?php wp_nonce_field('update-options'); ?>
		<input type="hidden" name="iks-service-slider-data" id="settings-src" value='<?php echo get_option('iks-service-slider-data') ?>'>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="iks-service-slider-data" />
		<?php echo get_submit_button() ?>
	</form>
</div>

<script id="slide-template" type="text/x-handlebars-template">
	<div class="slides__slide">
		<a class="control-button-inline up blue" onclick="replaceSlides({{ slideIndex }}, {{ slideIndex }}-1)">Выше</a>
		<a class="control-button-inline down blue" onclick="replaceSlides({{ slideIndex }}, {{ slideIndex }}+1)">Ниже</a>
		<a class="control-button-inline remove red" onclick="removeSlide({{ slideIndex }})">Удалить</a>
<!--		<textarea style="height: 200px" onchange="settings[{{slideIndex}}].text = this.value; putSettings(settings)" placeholder="Содержание">{{ text }}</textarea>-->
<!--    --><?php //wp_editor("{{ text }}", "{{  }}") ?>
	</div>
</script>

<style>
	.slides {
		width: 100%;
		max-width: 600px;
		margin-bottom: 20px;
	}
	.slides input,
	.slides textarea {
		margin: 20px 20px 0;
		width: 90%;
	}

	.slides__slide {
		min-height: 20px;
		background: #ddd;
		padding: 20px;
		margin-top: 10px;
	}
	.slides__slide:nth-child(2n) {
		background: #ccc;
	}
	.slides__slide h3 {
		margin-left: 20px;
		margin-bottom: 0;
	}
	.slides__slide:first-child > .control-button-inline.up,
	.slides__slide:last-child > .control-button-inline.down {
		display: none;
	}
	.control-button-inline {
		cursor: pointer;
		margin-left: 20px;
	}
	.control-button-inline.blue {
		color: rgb(50, 50, 255);
		border-bottom: dotted rgb(50, 50, 255) 1px;
	}

	.control-button-inline.red {
		color: #C00;
		border-bottom: dotted #C00 1px;
	}
</style>

<script src="<?php echo get_template_directory_uri() ?>/js/handlebars.js"></script>
<script defer>
  let settings = getSettings();
  updateHTML(settings);

  function updateHTML(settings) {
    let target = document.querySelector(".slides");
    target.innerHTML = "";

    let slideSource = document.getElementById("slide-template").innerHTML;

    let slideTemplate = Handlebars.compile(slideSource);

    for (let block in settings) {
      target.innerHTML += slideTemplate({
        text: settings[block].text,
        slideIndex: block || 0
      });
    }
  }

  function addSlide() {
    settings.push({
      text: "Текст нового слайда"
    });

    putSettings(settings);
    updateHTML(settings);
  }

  function removeSlide(blockIndex) {
    settings.splice(blockIndex, 1);
    putSettings(settings);
    updateHTML(settings);
  }


  function replaceSlides(b1, b2) {
    if (b1 < 0 || b2 < 0 || b1 >= settings.length || b2 >= settings.length) return;
    let buffer = settings[b1];
    settings[b1] = settings[b2];
    settings[b2] = buffer;
    putSettings(settings);
    updateHTML(settings);
  }

  function putSettings(settings) {
    document.getElementById('settings-src').value = JSON.stringify(settings);
  }
  function getSettings() {
    return JSON.parse(document.getElementById('settings-src').value || "[]");
  }
</script>