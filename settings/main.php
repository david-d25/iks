<?php
add_option("iks-company-phone", "8 800 123-45-67");
add_option("iks-instagram", "icslawyer");
add_option("iks-instagram-client-id", "xxxxxxxxxxxxxxxxxxxxxxxxxx");
add_option("iks-instagram-secret", "xxxxxxxxxxxxxxxxxxxxxxxxxx");
add_option("iks-instagram-access-token", "xxxxxxxxxxxxxxxxxxxxxxxxxx");
add_option("iks-recaptcha-sitekey", "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI");
add_option("iks-recaptcha-secret", "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe");
add_option("iks-whatsapp", "");
add_option("iks-skype", "");
add_option("iks-youtube", "");
add_option("iks-vacancy-email", "");
?>
<div class="wrap">
	<h2>
		<?php echo get_admin_page_title() ?>
	</h2>
	<form method="post" action="options.php">
	  <?php wp_nonce_field('update-options'); ?>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Номер телефона компании</th>
        <td>
          <input type="text" name="iks-company-phone" value="<?php echo get_option('iks-company-phone'); ?>" />
        </td>
      </tr>
    </table>
    <hr>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Номер WhatsApp (только цифры)</th>
        <td>
          <input type="text" name="iks-whatsapp" value="<?php echo get_option('iks-whatsapp'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Аккаунт Skype</th>
        <td>
          <input type="text" name="iks-skype" value="<?php echo get_option('iks-skype'); ?>" />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Ссылка YouTube</th>
        <td>
          <input type="text" name="iks-youtube" value="<?php echo get_option('iks-youtube'); ?>" />
        </td>
      </tr>
    </table>
    <hr>

		<table class="form-table">

      <tr valign="top">
        <th scope="row">Аккаунт Instagram</th>
        <td>
          <input type="text" name="iks-instagram" value="<?php echo get_option('iks-instagram'); ?>" />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">ID клиента для доступа к Instagram API</th>
        <td>
          <input type="text" name="iks-instagram-client-id" value="<?php echo get_option('iks-instagram-client-id'); ?>" />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Секретный ключ для доступа к Instagram API</th>
        <td>
          <input type="password" name="iks-instagram-secret" value="<?php echo get_option('iks-instagram-secret'); ?>" />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Токен для доступа к Instagram API</th>
        <td>
          <input type="password" name="iks-instagram-access-token" value="<?php echo get_option('iks-instagram-access-token'); ?>" />
        </td>
      </tr>

    </table>
    <hr>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Ключ сайта reCAPTCHA</th>
        <td>
          <input type="text" name="iks-recaptcha-sitekey" value="<?php echo get_option('iks-recaptcha-sitekey'); ?>" />
        </td>
      </tr>

      <tr valign="top">
        <th scope="row">Секретный код reCAPTCHA</th>
        <td>
          <input type="password" name="iks-recaptcha-secret" value="<?php echo get_option('iks-recaptcha-secret'); ?>" />
        </td>
      </tr>
    </table>
    <hr>

    <table class="form-table">
      <tr valign="top">
        <th scope="row">Куда отправлять заявки на вакансии (EMail)</th>
        <td>
          <input type="text" name="iks-vacancy-email" value="<?php echo get_option('iks-vacancy-email'); ?>" />
        </td>
      </tr>
    </table>

    <input type="hidden" name="action" value="update" />

    <input type="hidden" name="page_options" value="
      iks-company-phone,
      iks-instagram,
      iks-vacancy-email,
      iks-instagram-client-id,
      iks-instagram-secret,
      iks-instagram-access-token,
      iks-recaptcha-sitekey,
      iks-recaptcha-secret,
      iks-whatsapp,
      iks-skype,
      iks-youtube
    "/>

	  <?php echo get_submit_button() ?>
  </form>
</div>

<style>
  tr > th,
  tr > td {
    padding-top: 8px !important;
  }
</style>