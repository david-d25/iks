<?php
add_option("iks-main-menu-data", "[]");
?>
<div class="wrap">
  <h2>
    <?php echo get_admin_page_title() ?>
  </h2>
  <h3>Меню сайта</h3>
  <p style="color: grey">Незаполненные блоки или пункты меню тоже заполняют собой пространство. Используйте это, чтобы создавать пустые участки в меню.</p>
  <div class="menu-items"></div>
  <input type="button" class="button add-menu-item-btn" onclick="addSlide()" value="Добавить слайд">
  <form method="post" action="options.php">
    <?php wp_nonce_field('update-options'); ?>
    <input type="hidden" name="iks-main-menu-data" id="settings-src" value='<?php echo get_option('iks-main-menu-data') ?>'>
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="iks-main-menu-data" />
    <?php echo get_submit_button() ?>
  </form>
</div>

<script id="menu-template" type="text/x-handlebars-template">
  <div class="menu-items__item">
    <a class="control-button-inline up blue" onclick="replaceMenuItems({{ menuItemIndex }}, {{ menuItemIndex }}-1)">Выше</a>
    <a class="control-button-inline down blue" onclick="replaceMenuItems({{ menuItemIndex }}, {{ menuItemIndex }}+1)">Ниже</a>
    <a class="control-button-inline remove red" onclick="removeMenuItem({{ menuItemIndex }})">Удалить</a>
    <input type="text" onchange="settings[{{menuItemIndex}}].blockTitle = this.value; putSettings(settings)" value="{{ blockTitle }}" placeholder="Заголовок блока меню">
    {{#each itemlinks}}
    <p>
      <input type="text" onchange="settings[{{../menuItemIndex}}].itemlinks[{{@index}}].title = this.value; putSettings(settings)" value="{{ title }}" placeholder="Название пункта меню">
      <input type="text" style="margin-top: 5px" onchange="settings[{{../menuItemIndex}}]. itemlinks[{{@index}}].link = this.value; putSettings(settings)" value="{{ link }}" placeholder="Ссылка пункта меню">
    </p>
    {{~/each}}
  </div>
</script>

<style>
  .menu-items {
    width: 100%;
    max-width: 600px;
    margin-bottom: 20px;
  }
  .menu-items input,
  .menu-items textarea {
    margin: 20px 20px 0;
    width: 90%;
  }

  .menu-items__item {
    min-height: 20px;
    background: #ddd;
    padding: 20px;
    margin-top: 10px;
  }
  .menu-items__item:nth-child(2n) {
    background: #ccc;
  }
  .menu-items__item h3 {
    margin-left: 20px;
    margin-bottom: 0;
  }
  .menu-items__item:first-child > .control-button-inline.up,
  .menu-items__item:last-child > .control-button-inline.down {
    display: none;
  }
  .control-button-inline {
    cursor: pointer;
    margin-left: 20px;
  }
  .control-button-inline.blue {
    color: rgb(50, 50, 255);
    border-bottom: dotted rgb(50, 50, 255) 1px;
  }

  .control-button-inline.red {
    color: #C00;
    border-bottom: dotted #C00 1px;
  }
</style>

<script src="<?php echo get_template_directory_uri() ?>/js/handlebars.js"></script>
<script defer>
  let settings = getSettings();
  updateHTML(settings);

  function updateHTML(settings) {
    let target = document.querySelector(".menu-items");
    target.innerHTML = "";

    let slideSource = document.getElementById("menu-template").innerHTML;

    let slideTemplate = Handlebars.compile(slideSource);

    for (let block in settings) {
      target.innerHTML += slideTemplate({
        blockTitle: settings[block].blockTitle,
        itemlinks: settings[block].itemlinks,
        menuItemIndex: block || 0
      });
    }
  }

  function addSlide() {
    settings.push({
      blockTitle: "",
      itemlinks: [
        {title: "", link: ""},
        {title: "", link: ""},
        {title: "", link: ""},
        {title: "", link: ""},
        {title: "", link: ""}
      ]
    });

    putSettings(settings);
    updateHTML(settings);
  }

  function removeMenuItem(blockIndex) {
    settings.splice(blockIndex, 1);
    putSettings(settings);
    updateHTML(settings);
  }


  function replaceMenuItems(b1, b2) {
    if (b1 < 0 || b2 < 0 || b1 >= settings.length || b2 >= settings.length) return;
    let buffer = settings[b1];
    settings[b1] = settings[b2];
    settings[b2] = buffer;
    putSettings(settings);
    updateHTML(settings);
  }

  function putSettings(settings) {
    document.getElementById('settings-src').value = JSON.stringify(settings);
  }
  function getSettings() {
    return JSON.parse(document.getElementById('settings-src').value || "[]");
  }
</script>