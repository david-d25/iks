<?php
require_once("../../../../wp-load.php");

$url = "https://www.google.com/recaptcha/api/siteverify";
$data = array('secret' => get_option("iks-recaptcha-secret"), 'response' => $_POST['token']);
$options = array(
  'http' => array(
    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
    'method'  => 'POST',
    'content' => http_build_query($data)
  )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
function isValidEmail($email) {
  return filter_var($email, FILTER_VALIDATE_EMAIL);
}
if (!isValidEmail($_POST["email"])) {
  header("HTTP/1.1 512 Email is not valid");
} else if (json_decode($result)->success) {
  $to = $_POST["email"];
  $from = 'no-reply@example.com';
  $fromName = 'TEST SENDER';
  $subject = 'TEST SUBJECT';
  $file = "../style.css";
  $filename = "style.css";
  $htmlContent = '<h1>PHP Email with Attachment by CodexWorld</h1>
    <p>This email has sent from PHP script with attachment.</p>';

  //send email
  $mail = @mail($to, $subject, $htmlContent, $headers);

  //email sending status
  if ($mail) {
    header("HTTP/1.1 200 OK");
  } else {
    header("HTTP/1.1 514 Couldn't send");
  }
} else {
  header("HTTP/1.1 513 Captcha failed");
}