<?php
require_once("../../../../wp-load.php");

$index = $_POST['index'];
$query = $_POST['query'];

function monthName($number, $genitive) {
  switch ($number) {
    case 1: return $genitive ? "января" : "январь";
    case 2: return $genitive ? "февраля" : "февраль";
    case 3: return $genitive ? "марта" : "март";
    case 4: return $genitive ? "апреля" : "апрель";
    case 5: return $genitive ? "мая" : "май";
    case 6: return $genitive ? "июня" : "июнь";
    case 7: return $genitive ? "июля" : "июль";
    case 8: return $genitive ? "августа" : "август";
    case 9: return $genitive ? "сентября" : "сентябрь";
    case 10: return $genitive ? "октября" : "октябрь";
    case 11: return $genitive ? "ноября" : "ноябрь";
    case 12: return $genitive ? "декабря" : "декабрь";
  }
  return $number;
}

$instagram_username = get_option("iks-instagram");
$instagram_client_id   = get_option('iks-instagram-client-id');
$instagram_access_token = get_option('iks-instagram-access-token');

$min_year = +date("Y");
$min_month = +date("m") - $index;

while ($min_month <= 0) {
  $min_month += 12;
  $min_year--;
}

$max_year = $min_year;
$max_month = $min_month+1;
if ($max_month > 12) {
  $max_month = 1;
  $max_year++;
}

$min_timestamp = strtotime("$min_month/01/$min_year");
$max_timestamp = strtotime("$max_month/01/$max_year");

$instagram_cards = array();

$instagram_max_id = "";
while (true) {
  $finish_reached = false;
  $instagram_json = json_decode(file_get_contents("https://api.instagram.com/v1/users/self/media/recent?count=20&max_id=$instagram_max_id&access_token=$instagram_access_token"));
  foreach ($instagram_json->data as $photo_json) {
    if ($query != "" && (!$photo_json->caption || ($photo_json->caption && strpos($photo_json->caption->text, $query) === false))) continue;
    if ($photo_json->created_time < $min_timestamp) {
      $finish_reached = true;
      break;
    }
    else if ($photo_json->created_time > $max_timestamp) {
      continue;
    } else {
      $instagram_card = array();
      $instagram_card['timestamp'] = $photo_json->created_time;
      $instagram_card['image'] = $photo_json->images->standard_resolution->url;
      $instagram_card['marked'] = false;
      $instagram_card['link'] = $photo_json->link;
      $instagram_card['type'] = "card-instagram";
      $instagram_card['date'] = date("d", $photo_json->created_time) . " " . monthName(date("m", $photo_json->created_time), true);
      if ($photo_json->caption)
        $instagram_card['text'] = $photo_json->caption->text;
      array_push($instagram_cards, $instagram_card);
    }
  }
  if ($finish_reached || count($instagram_json->data) < 20) break;
  else $instagram_max_id = $instagram_json->data[count($instagram_json->data)-1]->id;
}

$posts_query = new WP_Query(array(
  "post_type" => array("post", "review"),
  "numberposts" => "-1",
  'orderby' => 'date',
  'date_query' => array(
    array(
      'after'     => date("d-m-Y", $min_timestamp),
      'before'    => date("d-m-Y", $max_timestamp),
      'inclusive' => true,
    ),
  ),
  "s" => $query
));

$post_cards = array();

foreach ($posts_query->posts as $post) {
  $post_card = array();
  $post_card['marked'] = true;
  if ($post->post_type == "post")
    $post_card['image'] = get_post_meta($post->ID, "post-illustration-uri", true);
  else if ($post->post_type == "review")
    $post_card['image'] = get_post_meta($post->ID, "review-thumbnail-uri", true);
  $post_card['timestamp'] = get_the_date("U", $post->ID);
  $post_card['text'] = get_the_title($post->ID);

  if (has_excerpt($post->ID))
    $post_card['description'] = get_the_excerpt($post->ID);

  $post_card['type'] = "card-" . $post->post_type;

  if ($post->post_type == "post")
    $post_card['link'] = get_permalink($post->ID);

  $post_card['date'] = date("d", get_the_date("U", $post->ID)) . " " . monthName(date("m", get_the_date("U", $post->ID)), true);
  array_push($post_cards, $post_card);
}

$cards = array();

while (count($post_cards) + count($instagram_cards) > 0) {
  if (count($instagram_cards) == 0)
    array_push($cards, array_shift($post_cards));
  else if (count($post_cards) == 0)
    array_push($cards, array_shift($instagram_cards));
  else if ($post_cards[0]['timestamp'] > $instagram_cards[0]['timestamp'])
    array_push($cards, array_shift($post_cards));
  else
    array_push($cards, array_shift($instagram_cards));
}
?>

<?php if (count($cards) > 0): ?>
  <section class="media <?php if ($index % 2 == 0) echo 'bg-color'; else echo 'bg-white' ?>">
    <div class="container">
      <h2><?php echo monthName(date("m", $min_timestamp), false) . " " . date("Y", $min_timestamp) ?></h2>
      <div class="card-columns mobile-slider">
        <?php foreach ($cards as $card): ?>
          <a <?php if ($card['link']) echo "href=\"" . $card['link'] . "\"" ?> style="text-decoration: inherit; color: inherit;">
            <div class="card <?php if ($card['marked']) echo "marked "; echo $card['type'] ?> wow fadeIn">
              <?php if ($card['image']): ?>
                <img class="card_img" src="<?php echo $card['image'] ?>" alt="">
              <?php endif; ?>
              <span class="card-date"><?php echo $card['date'] ?></span>
              <?php if ($card['text']): ?>
                <p class="card_text"><?php echo $card['text'] ?></p>
              <?php endif ?>
              <?php if ($card['description']): ?>
                <p class="card_description"><?php echo $card['description'] ?></p>
              <?php endif ?>
            </div>
          </a>
        <?php endforeach; ?>
      </div>
      <?php if ($index == 0): ?>
        <form action="#">
          <p>Раз в неделю будем присылать вам письмо со статьями наших экспертов</p>
          <input type="text" placeholder="Ваш телефон">
          <button> Подписаться</button>
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
            <label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="#">политикой конфиденциальности</a></label>
          </div>
        </form>
      <?php endif; ?>
  </section>
<?php endif; ?>