<?php
/*
Template Name: Press Page
*/
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <title>Team</title>
</head>
<body>

<header>
  <div class="alternative-header">
    <div class="container">
      <div class="row">
        <div class="col-12 banner">
          <img src="<?php echo get_template_directory_uri() ?>/img/alternative-banner2.png" alt="">
          <h1>Пресс-центр</h1>
        </div>
	      <?php
	      $currentPageSlug = 'press';
	      include 'templates/statics-navbar.php';
	      ?>
      </div>
    </div>
  </div>
</header>

<section class="press">
  <div class="container">
    <h2>Сотрудники пресс-службы ICS будут рады ответить на все вопросы</h2>
    <p>Звоните: <a href="#">7 (495) 782-82-46</a></p>
    <p>Пишите: <a href="#">info@consult-systems.ru</a></p>
    <div class="links">
      <p>Читайте материалы на сайте:</p>
      <ul>
        <li><a href="#">Интерьвю</a></li>
        <li><a href="#">Кейсы</a></li>
        <li><a href="#">Блог</a></li>
        <li><a href="#">Комментарии</a></li>
        <li><a href="#">Статьи</a></li>
        <li><a href="#">Пресс-релизы</a></li>
        <li><a href="#">Конференции</a></li>
        <li><a href="#">Круглые столы</a></li>
      </ul>
    </div>
  </div>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>