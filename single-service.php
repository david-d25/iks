<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />
</head>
<body>
<div class="wrapper">

  <?php
  get_header();
  ?>
  <div class="form-header">
    <div class="container banner" style="background-image: url(<?php echo get_post_meta(get_the_ID(), "illustration-uri", true) ?>)">
      <div class="row">
        <div class="col-xl-12 head wow fadeIn">
          <h1><?php echo str_replace(' | ', '<br>', get_the_title()); ?></h1>
          <p><?php echo str_replace(' | ', '<br>', get_post_meta(get_the_ID(), "service-subtitle", true)); ?></p>
          <a href="<?php echo get_post_meta(get_the_ID(), "online-payment-link", true) ?>">Оплатить услугу онлайн</a>
        </div>
        <div class="col-xl-12 wow fadeIn">
          <form action="#">
            <p class="description">Специалист перезвонит в 11:25 и ответит на все вопросы</p>
            <form action="#">
              <div class="form-group">
                <input id="number" type="tel" placeholder="Ваш телефон">
                <button>Перезвоните мне</button>
              </div>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
                <label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="#">политикой конфиденциальности</a></label>
              </div>
            </form>
          </form>
        </div>
      </div>
    </div>
  </div>


  <!-- Защищаем интересы клиентов -->
  <section class="info-interests">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <h2 class="wow fadeIn"><span class="line">Защищаем</span><span class="line">интересы</span>
            <span class="line">клиентов</span></h2>
        </div>
        <div class="col-lg-12 col-xl-6 wow fadeIn fix-bullets">
          <?php echo get_post_meta(get_the_ID(), "protect-left", true) ?>
        </div>
        <div class="col-lg-12 col-xl-6 wow fadeIn fix-bullets">
          <?php echo get_post_meta(get_the_ID(), "protect-right", true) ?>
        </div>
      </div>
    </div>
  </section>

  <!-- Как мы работаем -->
  <section class="info-worked">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-12">
          <h2 class="wow fadeIn"><span class="line">Как мы</span><span class="line">работаем</span></h2>
        </div>
        <div class="col-xl-9">
          <?php
          $court1 = get_post_meta(get_the_ID(), "work-court-1", true);
          $court2 = get_post_meta(get_the_ID(), "work-court-2", true);
          $court3 = get_post_meta(get_the_ID(), "work-court-3", true);
          $court4 = get_post_meta(get_the_ID(), "work-court-4", true);
          $fas1 = get_post_meta(get_the_ID(), "work-FAS-1", true);
          $fas2 = get_post_meta(get_the_ID(), "work-FAS-2", true);
          $fas3 = get_post_meta(get_the_ID(), "work-FAS-3", true);
          $fas4 = get_post_meta(get_the_ID(), "work-FAS-4", true);
          if (($court1 || $court2 || $court3 || $court4) && ($fas1 || $fas2 || $fas3 || $fas4)):
          ?>
          <div class="nav-wrapper">
            <nav>
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Представление в суде</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Предствление в фас</a>
                </li>
              </ul>
            </nav>
          </div>
          <?php endif; ?>

          <div class="tab-content" id="pills-tabContent">
            <?php if ($court1 || $court2 || $court3 || $court4): ?>
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

              <?php if ($court1): ?>
              <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <div class="col-sm-2">
                  <img class="number" src="<?php echo get_template_directory_uri() ?>/img/1.png" alt="">
                </div>
                <div class="col-sm-10">
                  <?php echo $court1 ?>
                  <div class="buttons-group">

                    <?php if (get_post_meta(get_the_ID(), "work-court-leave-request-button", true)): ?>
                    <a class="button" href="#">оставить заявку</a>
                    <?php endif; ?>

                    <?php if (get_post_meta(get_the_ID(), "work-court-contract-uri", true)): ?>
                    <a class="download" href="<?php echo get_post_meta(get_the_ID(), "work-court-contract-uri", true) ?>"><img src="<?php echo wp_get_attachment_image_src(pippin_get_image_id(get_post_meta(get_the_ID(), "work-court-contract-uri", true)))[0] ?>" alt=""> <p>Скачать<br>фрагмент<br>договора</p></a>
                    <?php endif; ?>

                  </div>
                </div>
              </div>
              <?php endif; ?>

              <?php if ($court2): ?>
              <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <div class="col-sm-2">
                  <img class="number" src="<?php echo get_template_directory_uri() ?>/img/2.png" alt="">
                </div>
                <div class="col-sm-10">
                  <?php echo $court1 ?>
                  <div class="info-block">
                    <img src="<?php echo get_template_directory_uri() ?>/img/lock.png" alt="">
                    <p>Прописываем условия конфиденциальности в каждом договоре </p>
                  </div>
                </div>
              </div>
              <?php endif; ?>

              <?php if ($court3): ?>
              <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <div class="col-sm-2">
                  <img class="number" src="<?php echo get_template_directory_uri() ?>/img/3.png" alt="">
                </div>
                <div class="col-sm-10">
                  <?php echo $court3 ?>
                </div>
              </div>
              <?php endif; ?>

              <?php if ($court4): ?>
              <div class="row step wow fadeIn" style="visibility: hidden; animation-name: none;">
                <div class="col-sm-2">
                  <img class="number" src="<?php echo get_template_directory_uri() ?>/img/4.png" alt="">
                </div>
                <div class="col-sm-10">
                  <?php echo $court3 ?>
                </div>
              </div>
              <?php endif; ?>
            </div>
            <?php endif; ?>

            <?php if ($fas1 || $fas2 || $fas3 || $fas4): ?>
            <div class="tab-pane fade <?php if (!($court1 || $court2 || $court3 || $court4)) echo "show active"; ?>" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
              <?php if ($fas1): ?>
                <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                  <div class="col-sm-2">
                    <img class="number" src="<?php echo get_template_directory_uri() ?>/img/1.png" alt="">
                  </div>
                  <div class="col-sm-10">
                    <?php echo $fas1 ?>
                    <div class="buttons-group">

                      <?php if (get_post_meta(get_the_ID(), "work-FAS-leave-request-button", true)): ?>
                        <a class="button" href="#">оставить заявку</a>
                      <?php endif; ?>

                      <?php if (get_post_meta(get_the_ID(), "work-FAS-contract-uri", true)): ?>
                        <a class="download" href="#"><img src="<?php get_post_meta(get_the_ID(), "work-FAS-contract-uri", true) ?>" alt=""> <p>Скачать<br>фрагмент<br>договора</p></a>
                      <?php endif; ?>

                    </div>
                  </div>
                </div>
              <?php endif; ?>

              <?php if ($fas2): ?>
                <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                  <div class="col-sm-2">
                    <img class="number" src="<?php echo get_template_directory_uri() ?>/img/2.png" alt="">
                  </div>
                  <div class="col-sm-10">
                    <?php echo $fas1 ?>
                    <div class="info-block">
                      <img src="<?php echo get_template_directory_uri() ?>/img/lock.png" alt="">
                      <p>Прописываем условия конфиденциальности в каждом договоре </p>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

              <?php if ($fas3): ?>
                <div class="row step wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                  <div class="col-sm-2">
                    <img class="number" src="<?php echo get_template_directory_uri() ?>/img/3.png" alt="">
                  </div>
                  <div class="col-sm-10">
                    <?php echo $fas3 ?>
                  </div>
                </div>
              <?php endif; ?>

              <?php if ($fas4): ?>
                <div class="row step wow fadeIn" style="visibility: hidden; animation-name: none;">
                  <div class="col-sm-2">
                    <img class="number" src="<?php echo get_template_directory_uri() ?>/img/4.png" alt="">
                  </div>
                  <div class="col-sm-10">
                    <?php echo $fas3 ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Почему стоит работать с нами -->
  <section class="info-advantages">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-xl-5">
          <h2 class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;"><span class="line">Почему</span><br><span class="line">стоит</span><br><span class="line">работать</span><br><span class="line">с нами</span></h2>
        </div>
        <div class="col-xl-7">
          <div class="row icons-group">
            <div class="col-xs-12 col-md-6 icon wow fadeIn" style="height: inherit;">
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/i_02.png" alt="">
              <span class="title">Добросовестность</span>
              <p>Информируем о перспективах проектов и предлагаем самый эффективный вариант разрешения спора</p>
            </div>
            <div class="col-xs-12 col-md-6 icon wow fadeIn" style="height: inherit;">
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/i_04.png" alt="">
              <span class="title">Прозрачность</span>
              <p>В формировании цены за услуги. Никаких подводных камней и скрытых платежей</p>
            </div>
            <div class="col-xs-12 col-md-6 icon wow fadeIn" style="height: inherit;">
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/i_01.png" alt="">
              <span class="title">Доступность</span>
              <p>Общаемся с клиентом очно, дистанционно, по телефону, с помощью мессенджеров либо почты</p>
            </div>
            <div class="col-xs-12 col-md-6 icon wow fadeIn" style="height: inherit;">
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/i_03.png" alt="">
              <span class="title">Профессионализм</span>
              <p>Каждый наш специалист — эксперт в соответствую-щей области права</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include 'templates/reviews.php' ?>

  <!-- Примеры из практики -->
  <section class="info-practice">
    <div class="container">
      <div class="row">
        <div class="col-xl-6">
          <h2 class="wow fadeIn"><span class="line">Примеры</span></br>
            <span class="line">из практики</span></h2>
          <p class="description"><?php echo get_post_meta(get_the_ID(), "service-practices-subtitle", true) ?></p>
        </div>
        <div class="col-xl-6">
          <div class="practice-slider">
            <?php
            $data = json_decode(get_option('iks-service-slider-data'), true);
            foreach ($data as $key => $value):
              ?>
              <div class="item">
                <?php echo $value['text'] ?>
                <div class="client">
                  <img src="<?php echo get_template_directory_uri() ?>/img/client.png" alt="">
                  <p>Имя Клиента защищено положением о конфиденциальности нашего договора об оказании услуг</p>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Об услуге -->
  <section class="info-about">
    <div class="container">
      <div class="row">
        <div class="col-xl-7">
          <h2 class="wow fadeIn"><span class="line">Об услуге</span></h2>
          <?php echo get_post_meta(get_the_ID(), "service-about-content", true) ?>
        </div>
        <div class="col-xl-5">
          <form id="send-price" method="POST">
            <p><strong>Скачайте прайс и презентацию ICS</strong> чтобы больше узнать об услугах  нашей компании. Без спама</p>
            <input type="email" name="email" id="email" placeholder="E-mail">
            <button class="g-recaptcha" data-sitekey="<?php echo get_option("iks-recaptcha-sitekey") ?>" data-callback='onSubmit'>Получить прайс</button>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customControlInline" checked>
              <label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="/privacy-policy">политикой конфиденциальности</a></label>
            </div>

            <div style="opacity: .3; color: grey">
              This site is protected by reCAPTCHA and the Google
              <a style="color: white;" href="https://policies.google.com/privacy">Privacy Policy</a> and
              <a style="color: white;" href="https://policies.google.com/terms">Terms of Service</a> apply.
            </div>

          </form>
          <script src="https://www.google.com/recaptcha/api.js" async defer></script>
          <script>
            function onSubmit(token) {
              let xhr = new XMLHttpRequest();
              xhr.open("POST", "<?php echo get_template_directory_uri() ?>/backend/send-price.php", false);
              xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
              xhr.send(`email=${$("#send-price #email").val()}&token=${token}`);
              if (+xhr.status !== 200 || xhr.responseText.length > 0) {
                console.log( xhr.status + ': ' + xhr.statusText ); // FAILED
                console.log( xhr.responseText ); // FAILED
              } else {
                console.log( "SUCCESS: " + xhr.responseText); // SENT SUCCESS
              }
            }
          </script>
        </div>
      </div>
    </div>
  </section>

  <!-- Публикации в СМИ -->
  <section class="info-massmedia">
    <div class="container">
      <div class="col-xl-12 headline">
        <h2 class="wow fadeIn"><span class="line">Публикации</span><span class="line">в сми</span></h2>
        <a class="link wow fadeIn" href="/media">Все публикации</a>
      </div>
      <div class="col-xl-12">
        <div class="massmedia-slider">
          <?php
          for ($i = 1; $i <= 4; $i++):
            $publicationID = get_post_meta(get_the_ID(), "service-post-id-" . $i, true);
            if ($publicationID != -1):
              ?>
              <a class="item wow fadeIn" style="text-decoration: inherit; color: inherit" href="<?php echo get_the_permalink($publicationID) ?>">
                <img class="logo" src="<?php echo get_template_directory_uri() ?>/img/empty-logo.png" alt="Лого">
                <span class="title"><?php echo get_the_title($publicationID) ?></span>
                <p class="text"><?php if (has_excerpt($publicationID)) echo get_the_excerpt($publicationID) ?></p>
              </a>
            <?php endif; endfor; ?>
        </div>
      </div>
    </div>
  </section>

  <?php
  include "templates/contacts-section.php";
  get_footer();
  ?>
</div>

<style>
  .grecaptcha-badge {
    display: none;
  }
</style>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>

<script>
  $(document).ready(function(){
    $(".fix-bullets ul li").each(function (index) {
      $(this).html(`<span>${$(this).html()}</span>`);
    });
    $('.practice-slider').slick({
      dots: true,
      arrows: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
    });

    $('.massmedia-slider').slick({
      dots: false,
      arrows: false,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1140,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: false,
            dots: true
          }
        },
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            dots: true
          }
        },
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true
          }
        }
      ]
    });
  });
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

</script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.maskedinput.min.js"></script>
<script>
  $(function(){
    $("#number").mask("8(999) 999-9999");
  });
</script>

</body>
</html>