<?php

add_action('admin_menu', 'registerThemeMenu');

function registerThemeMenu() {
	add_menu_page(
		"Настройки IKS",
		"Настройки IKS",
		"manage_options",
		"iks-settings",
		"IKSSettingsMenu",
		""
	);

	add_pages_page(
		"Слайдер главной страницы",
		"Слайдер главной страницы",
		"manage_options",
		"iks-settings__main-slider",
		"mainSliderMenu"
	);

	add_pages_page(
		"Публикации на главной странице",
		"Публикации на главной странице",
		"manage_options",
		"iks-settings__main-posts",
		"postsMenu"
	);

  add_pages_page(
    "Ссылки главной страницы",
    "Ссылки главной страницы",
    "manage_options",
    "iks-settings__main-links",
    "mainLinks"
  );

	add_submenu_page(
		"iks-settings",
		"Слайдер страницы услуги",
		"Слайдер страницы услуги",
		"manage_options",
		"iks-settings__service-slider",
		"serviceSlider"
	);

  add_submenu_page(
    "iks-settings",
    "Меню сайта",
    "Меню сайта",
    "manage_options",
    "iks-settings_main-menu",
    "mainMenu"
  );

	add_submenu_page(
		"iks-settings",
		"Страница вакансий",
		"Страница вакансий",
		"manage_options",
		"iks-settings__vacancies-page",
		"vacanciesPage"
	);
  // TODO: настройки email
	function IKSSettingsMenu() {
		include 'settings/main.php';
	}

	function mainSliderMenu() {
		include 'settings/main-slider.php';
	}

	function postsMenu() {
		include 'settings/posts.php';
	}

	function serviceSlider() {
		include 'settings/service-slider.php';
	}

	function vacanciesPage() {
		include 'settings/vacancies-page.php';
	}

	function mainMenu() {
	  include 'settings/main-menu.php';
  }

  function mainLinks() {
	  include 'settings/main-links.php';
  }
}

include "settings/posttypes/service.php";
include "settings/posttypes/review.php";
include "settings/posttypes/employee.php";
include "settings/posttypes/post.php";
include "settings/posttypes/vacancy.php";
include "settings/posttypes/service-block.php";

function pippin_get_image_id($image_url) {
  global $wpdb;
  $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
  return $attachment[0];
}