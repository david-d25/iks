<div class="col-12 menu wow fadeIn">
	<nav>
		<ul>
			<li><a class="link <?php if ($currentPageSlug == 'about') echo 'activ' ?>" href="/about">О компании</a></li>
			<li><a class="link <?php if ($currentPageSlug == 'team') echo 'activ' ?>" href="/team">Команда</a></li>
			<li><a class="link <?php if ($currentPageSlug == 'jobs') echo 'activ' ?>" href="/jobs">Вакансии</a></li>
			<li><a class="link <?php if ($currentPageSlug == 'contacts') echo 'activ' ?>" href="/contacts">Контакты</a></li>
			<li><a class="link <?php if ($currentPageSlug == 'press') echo 'activ' ?>" href="/press">Пресс-центр</a></li>
		</ul>
	</nav>
</div>