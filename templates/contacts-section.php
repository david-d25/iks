<section class="contacts">
	<img class="logo" src="<?php echo get_template_directory_uri() ?>/img/icons/logo-footer.png" alt="">
	<div class="social-icons">
		<ul>
			<li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/whatsapp.svg" alt=""></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/skype.svg" alt=""></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/youtube.svg" alt=""></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/instagram.svg" alt=""></a></li>
		</ul>
	</div>
	<span class="headline">Связаться с нами</span>
	<p class="description">Опытный юрист перезвонит в 11:25 и ответит на вопросы</p>
	<form action="#">
		<input id="number" type="tel" placeholder="Ваш телефон">
		<button>Перезвоните мне</button>
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input" id="customControlInline" checked>
			<label class="custom-control-label" for="customControlInline">Нажимая на кнопку «Отправить заявку», вы соглашаетесь с <a href="#">политикой конфиденциальности</a></label>
		</div>
	</form>
</section>