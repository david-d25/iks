<?php
$data = json_decode(get_option('iks-mainpage-slider-data'), true);
?>
<div class="slider">
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
      <?php foreach ($data as $key => $value): ?>
			<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $key ?>" <?php if ($key == 0) echo 'class="active"' ?>></li>
			<?php endforeach; ?>
		</ol>
		<div class="carousel-inner">
      <?php foreach ($data as $key => $value): ?>
			<div class="carousel-item <?php if ($key == 0) echo 'active' ?>">
				<img class="d-block h-100" style="background-size: cover;" src="<?php echo get_option("iks-mainpage-slider-bg-uri") ?>" alt="Slide">
				<div class="carousel-caption">
					<h1 class="wow fadeIn"><?php echo $value['title'] ?></h1>
					<p class="wow fadeIn"><?php echo $value['text'] ?></p>
					<a class="wow fadeIn" href="<?php echo $value['buttonLink'] ?>"><?php echo $value['buttonCaption'] ?></a>
					<div id="scene">
						<img data-depth="0.2" src="<?php echo $value['illustration'] ?>" alt="#">
					</div>
				</div>
			</div>
      <?php endforeach; ?>
    </div>
	</div>
</div>