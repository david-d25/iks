<section>
	<div class="reviews row">
		<div class="col-lg-12 order-2 col-xl-6">
			<div class="row justify-content-end mobile-slider">
				<?php
				$reviews = get_posts(array("post_type" => "review", "numberposts" => "-1"));
				for ($i = 0; $i < min(5, count($reviews)); $i++):
					?>
					<div class="col-md-4 col-xl-4 col-6 mb-5 wow fadeIn" data-toggle="modal" data-target="#exampleModal">
						<img src="<?php echo get_post_meta($reviews[$i]->ID, "review-thumbnail-uri", true) ?>" width="100%" alt="Отзыв">
					</div>
				<?php endfor; ?>
				<?php
				if (count($reviews) > 5)
					for ($i = 5; $i < count($reviews); $i++):
						?>
						<div class="col-md-4 col-xl-4 col-6 mb-5 wow fadeIn review hidden" data-toggle="modal" data-target="#exampleModal">
							<img src="<?php echo get_post_meta($reviews[$i]->ID, "review-thumbnail-uri", true) ?>" width="100%" alt="Отзыв">
						</div>
					<?php endfor; ?>
			</div>
		</div>
		<div class="col-lg-12 order-1 order-xl-2 col-xl-6">
			<h2 class="wow fadeIn"><span class="line">Более 200</span></br>
				<span class="line">довольных</span></br>
				<span class="line">клиентов</span></h2>
			<a class="link wow fadeIn" href="#" id="show-all-reviews-button" >Все отзывы</a>
		</div>
	</div>
</section>


<style>
  .review.hidden {
    display: none;
  }
</style>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#show-all-reviews-button").click((event) => {
      event.preventDefault();
      $(".review.hidden").removeClass("hidden");
    });

      if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }
  });
</script>