<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/services.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/demo.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/pater/pater.css" />
	<title>Article</title>
</head>
<body>
<?php get_header() ?>

<header>
	<div class="alternative-header article-header">
		<div class="container">
			<div class="row">
				<div class="col-12 banner">
					<img src="<?php echo get_post_meta(get_the_ID(), "post-illustration-uri", true) ?>" alt="">
				</div>
			</div>
		</div>
	</div>
</header>

<section class="article">
	<div class="container">
		<div class="row">
      <div class="col-2">
        <div class="author">
      <?php
      $authorID = get_post_meta(get_the_ID(), "post-author-id", true);
      if ($authorID != -1): ?>
					<img src="<?php echo get_post_meta($authorID, "employee-avatar-uri", true) ?>" style="max-width: 100%" alt="Avatar">
					<span class="name"><?php echo get_post_meta($authorID, "employee-name", true) ?></span>
					<p class="position"><?php echo get_post_meta($authorID, "employee-position", true) ?></p>
					<p class="company">Инвест Консалт Системс</p>
					<a href="#">11 статей</a> <!-- TODO: Статьи -->
      <?php endif; ?>
        </div>
      </div>
			<div class="col-8 cover-article">
				<h1><?php the_title() ?></h1>
				<div class="post-marks">
					<time><?php the_date("j F Y") ?></time>
          <?php foreach (wp_get_post_tags(get_the_ID()) as $tag)
                  echo "<span class='tag'>$tag->name</span>"
            ?>
				</div>

        <?php echo get_post_field('post_content', get_the_ID()); ?>

			</div>
			<div class="col-2">

        <?php
        $tags = "";
        foreach (wp_get_post_tags(get_the_ID()) as $tag)
          $tags .= "," . $tag->name;
        if (strlen($tags) > 0) $tags = substr($tags, 1);
        $tag_query = new WP_Query(array('tag
        ' => $tags, 'post__not_in' => array(get_the_ID())));
        if ($tag_query->have_posts()):
	        $post = $tag_query->get_posts()[0];
        ?>
				<div class="post-card">
					<a href="<?php echo $post->guid ?>" class="link-wrapper">
            <?php if (get_post_meta($post->ID, "post-illustration-uri", true)): ?>
						<img src="<?php echo get_post_meta($post->ID, "post-illustration-uri", true) ?>"  style="max-height: 500px; max-width: 100%;" alt="Illustration">
            <?php endif; ?>
						<p><?php echo $post->post_title ?></p>
						<time><?php echo get_the_time("j F Y", $post->ID) ?></time>
					</a>
				</div>
        <?php endif; ?>

        <?php
        $anotherPostID = get_post_meta(get_the_ID(), "post-another-post", true);
        if ($anotherPostID != -1):
        ?>
				<div class="service-card">
					<a href="<?php echo get_permalink($anotherPostID) ?>" class="link-wrapper">
						<img src="<?php echo get_post_meta($anotherPostID, "post-illustration-uri", true) ?>" alt="">
						<span class="title"><?php echo get_the_title($anotherPostID) ?></span>
						<p><?php if (has_excerpt($anotherPostID)) echo get_the_excerpt($anotherPostID) ?></p>
						<span class="transition">Подробнее</span>
            <?php var_dump("AnotherID: $anotherPostID") ?>
            <?php var_dump("ID: " . get_the_ID()) ?>
					</a>
				</div>
        <?php endif; ?>
			</div>
			<div class="col-12">
				<div class="left">
					<img src="<?php echo get_template_directory_uri() ?>/img/card-left.png" alt="">
					<span>Предыдущая статья</span>
					<p>Как не попасть в турбину самолета. Руководство выживание для голубей, гусей и других перелетных птиц</p>
					<time>30 апреля 2018</time>
				</div>
				<div class="right">
					<img src="<?php echo get_template_directory_uri() ?>/img/card-right.png" alt="">
					<span>Предыдущая статья</span>
					<p>Как не попасть в турбину самолета. Руководство выживание для голубей, гусей и других перелетных птиц</p>
					<time>30 апреля 2018</time>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
include 'templates/contacts-section.php';
get_footer();
?>

<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/headhesive.min.js"></script>
<script>
  new WOW().init();

  var options = {
    offset: 1080
  };

  var header = new Headhesive('.header', options);
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    if(screen.width<=768) {
      $('.mobile-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });
    }
  });


</script>

<script>
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/demo.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/easings.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/demo5.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.maskedinput.min.js"></script>
<script>
  $(function(){
    $("#number").mask("8(999) 999-9999");
  });
</script></body>
</html>